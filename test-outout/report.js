$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Order.feature");
formatter.feature({
  "line": 2,
  "name": "Order",
  "description": "",
  "id": "order",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Order"
    }
  ]
});
formatter.before({
  "duration": 22877400,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "user access to website",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "User opens the Website",
  "keyword": "Given "
});
formatter.match({
  "location": "Step_SignIn.user_opens_the_Website()"
});
formatter.result({
  "duration": 70574795000,
  "status": "passed"
});
formatter.scenario({
  "line": 744,
  "name": "Verify user is able to add items from menu after clicking on Add more items and place order successfully for delivery order",
  "description": "",
  "id": "order;verify-user-is-able-to-add-items-from-menu-after-clicking-on-add-more-items-and-place-order-successfully-for-delivery-order",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 742,
      "name": "@OrderDebug"
    },
    {
      "line": 743,
      "name": "@DevOrder"
    }
  ]
});
formatter.step({
  "line": 745,
  "name": "user clicks on Order button",
  "keyword": "Given "
});
formatter.step({
  "line": 746,
  "name": "user enters the address",
  "rows": [
    {
      "cells": [
        "address",
        "4319 Main St Philadelphia, PA"
      ],
      "line": 747
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 748,
  "name": "user clicks on Delivery button",
  "keyword": "And "
});
formatter.step({
  "line": 749,
  "name": "user select Date and Time from Calendar",
  "keyword": "And "
});
formatter.step({
  "line": 750,
  "name": "user clicks on Continue",
  "keyword": "And "
});
formatter.step({
  "line": 751,
  "name": "On Menu Page click on The Insomniac",
  "keyword": "And "
});
formatter.step({
  "line": 752,
  "name": "user clicks on add product",
  "keyword": "And "
});
formatter.step({
  "line": 753,
  "name": "user goes to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 754,
  "name": "user clicks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 755,
  "name": "user clicks on Add more items button on Checkout page",
  "keyword": "And "
});
formatter.step({
  "line": 756,
  "name": "On Menu Page click on The Major Rager",
  "keyword": "And "
});
formatter.step({
  "line": 757,
  "name": "user clicks on add product",
  "keyword": "And "
});
formatter.step({
  "line": 758,
  "name": "user goes to the cart again",
  "keyword": "And "
});
formatter.step({
  "line": 759,
  "name": "product added after clicking add more items button should be present in the cart",
  "keyword": "Then "
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Order_button()"
});
formatter.result({
  "duration": 31404851800,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_enters_the_address(DataTable)"
});
formatter.result({
  "duration": 13295103700,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Delivery_button()"
});
formatter.result({
  "duration": 11218755600,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_select_Date_and_Time_from_Calendar()"
});
formatter.result({
  "duration": 3293190100,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Continue()"
});
formatter.result({
  "duration": 333916800,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.on_Menu_Page_click_on_the_Insomniac()"
});
formatter.result({
  "duration": 23992447400,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_add_product()"
});
formatter.result({
  "duration": 909412700,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_goes_to_the_cart()"
});
formatter.result({
  "duration": 1950114200,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_the_checkout_button()"
});
formatter.result({
  "duration": 107125900,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Add_more_items_button_on_Checkout_page()"
});
formatter.result({
  "duration": 31361354900,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.major_rager()"
});
formatter.result({
  "duration": 5258875100,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_add_product()"
});
formatter.result({
  "duration": 678889800,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.go_to_cart_again()"
});
formatter.result({
  "duration": 84812200,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.add_more_items_button()"
});
formatter.result({
  "duration": 12253567700,
  "status": "passed"
});
formatter.after({
  "duration": 67600,
  "status": "passed"
});
});