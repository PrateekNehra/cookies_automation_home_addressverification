@Order
Feature: Order

  Background: user access to website
    Given User opens the Website
  
  #| url | https://5.stage.insomniacookies.com/ |
@DevOrder
  Scenario: Verify User is able to place an Delivery order with Credit card
  And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    

@DevOrder  
  Scenario: Verify User is able to place a Delivery order with Cash
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    

 
  Scenario: Verify User is able to place an Delivery order with School Cash
    Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  pa |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Shaurya Nigam    |
      | recepient phone  |       2345678981 |
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as School cash
    And user enters school cash number
      | School cash | 6020000000000000 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

@DevOrder
  Scenario: Verify User is able to place an Delivery order with Gift Card
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Shaurya Nigam    |
      | recepient phone  |       2345678981 |
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877652 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
 

  Scenario: Verify User is able to place a Pickup order with Credit card
    Given user clicks on Order button
    When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: Verify User is able to place a Pickup order with Cash
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: Verify User is able to place a Pickup order with School Cash
    Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  pa |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as School cash
    And user enters school cash number
      | School cash | 6020000000000000 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: Verify User is able to place a Pickup order with Gift Card
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877652 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    Scenario: Verify User is able to track an Delivery Order just after Ordering
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as cash
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Tracking ID should be displayed along with status
    
 
  Scenario: Verify User is able to track a Pickup Order just after Ordering
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877652 |
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Tracking ID should be displayed along with status


  Scenario: Verify loyalty point earned for Delivery total under 20$ calculates to 1$ = 1point
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South, Suite 200 Birmingham, AL, 35205 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning One points for this order
    

	  Scenario: Verify guest user is not able to add Cookie Dough to a Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cookie Dough button should not appear to guest user in the cart



  Scenario: Verify loyalty point earned for Delivery total above 20$ calculates to 1$ = 1.25 point
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South, Suite 200 Birmingham, AL, 35205 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning Twenty Nine points for this order
	

  Scenario: Verify loyalty point earned for Pickup total under 20$ calculates to 1$ = 1point
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South, Suite 200 Birmingham, AL, 35205 |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning One points for this order

  

  Scenario: Verify loyalty point earned for Pickup total above 20$ calculates to 1$ = 1.25 point
    Given user clicks on Order button
    When user enters the address
      | address | 217 Welch Ave., Suite 102 Ames, IA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning Twenty Nine points for this order

  Scenario: Verify guest user is not able to apply Product coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South, Suite 200 Birmingham, AL, 35205 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Product code | 6 Free Cookies |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear


  Scenario: Verify guest user is not able to apply Amount coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South, Suite 200 Birmingham, AL, 35205 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

@DevOrder
  Scenario: Verify guest user is not able to apply Percentage coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | CYBER |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear
 
 @DevOrder
  Scenario: Verify guest user is not able to apply Free Delivery coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear
    

  Scenario: Verify guest user is not able to apply Product coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Product code | 6 Free Cookies |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

@DevOrder
  Scenario: Verify guest user is not able to apply Amount coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: Verify guest user is not able to apply Percentage coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | CYBER |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: Verify guest user is not able to apply Free Delivery coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: Verify guest user is not able to place an Delivery order for past date
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender
    Then user should not be able to select back date

  Scenario: Verify guest user is not able to place an Pickup order for past date
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender
    Then user should not be able to select back date

  Scenario: Verify guest user is not able to place an Delivery order beyond 6 months
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender beyond six months but is unable to do so

  Scenario: Verify guest user is not able to place an Pickup order beyond 6 months
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender beyond six months but is unable to do so


  Scenario: Verify user is able to delete items from cart and add them again into it for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | University Plaza 4710 Vestal Parkway East Vestal, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user deleted the product
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then product should be present


  Scenario: Verify user is able to delete items from cart and add them again into it for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 304 West 14th Street New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user deleted the product
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then product should be present


  Scenario: Verify Upsell is displayed for the Delivery orders
    Given user clicks on Order button
    When user enters the address
      | address | University Plaza 4710 Vestal Parkway East Vestal, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then upsell should be displayed


  Scenario: Verify Upsell is displayed for the Pickup orders
    Given user clicks on Order button
    When user enters the address
      | address | 482 Third Avenue New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then upsell should be displayed


  Scenario: verify user is able to close menu page for every product
    Given user clicks on Order button
    When user enters the address
      | address | 482 Third Avenue New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on  close button
    Then product menu should close

@DevOrder
  Scenario: Verify that guest user is getting a pop up stating that Delivery instructions saved when entering data in delivery instruction textbox
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    Then User should get a pop up stating that Delivery instructions saved


  Scenario: Verify that guest user is getting a pop up stating that Message successfully added when entering data in Message for recipient textbox
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters message for recepient
      | message | This is for you |
    Then User should get a pop up stating that Message successfully added


  Scenario: Verify that user is getting pop up after adding product in cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    Then User should get a pop up stating Product Name added to cart


  Scenario: TC093- Verify guest user is able to update delivery time from the cart for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks Update button
    Then updated date & time should be displayed on the cart top

 @DevOrder
  Scenario: TC095- Verify guest user is able to update pickup time from the cart for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Pickup time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks Update button
    Then updated pickup date & time should be displayed on the cart top

  Scenario: Verify guest user is able to login from the cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on login button present in the cart
    When User enters valid Email and Password
      | username | snigam@judge.com |
      | password | password         |
    And Click on the Log In button
    Then User must get logged in and UserName should be displayed in the header

  Scenario: Verify guest user is able to set tip for an Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 228 E Clayton St, Athens GA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user selects tip
      | tip selected | 1.00 |
    And validation popup appears saying tip updated
    And user clicks on Place Order
    Then Order confirmation page should appear displaying tip in Order summary
      | tip displayed | $1.00 |

@DevOrder
  Scenario: TC127 - Verify that tracking ID present in confirmation mail should navigate to tracking page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as cash
    And user clicks on Place Order
    And user clicks on Tracking ID link on Order confirmation page
    Then Tracking ID should navigate to tracking page

  Scenario: TC129 - Verify that tracking ID present in confirmation mail should navigate to tracking page for pickup order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as cash
    And user clicks on Place Order
    And user clicks on Tracking ID link on Order confirmation page
    Then Tracking ID should navigate to tracking page

  #################################Add more Items button- Delivery Order############################################
  
  Scenario: Verify user is able to see  Add more items button on checkout page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then user should be able to see Add more items button on Checkout page


  Scenario: Verify user redirects to Menu page on clicking Add more items button on checkout page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    Then user should redirect to Menu page
@OrderDebug
@DevOrder
  Scenario: Verify user is able to add items from menu after clicking on Add more items and place order successfully for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart again
    Then product added after clicking add more items button should be present in the cart

  ################################Add more Items button- Pickup Order###################################################

  Scenario: Verify user is able to see  Add more items button on checkout page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then user should be able to see Add more items button on Checkout page


  Scenario: Verify user redirects to Menu page on clicking  Add more items button on checkout page for Pickup order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    Then user should redirect to Menu page
@DevOrder
  Scenario: Verify user is able to add items from menu after clicking on Add more items and place order successfully for Pickup order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
     And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart again
    Then product added after clicking add more items button should be present in the cart

  #########################
@DevOrder
  Scenario: Verify user can register just after placing Delivery order by clicking Register after checkout
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info along with new emailID
      | recepient name  | Theresa Ainsworth |
      | recepient phone |        2345678981 |
      | customer name   | Shaurya Nigam     |
      | customer phone  |        3457689024 |
    And user clicks radio button saying Register after Checkout
    And user enters password & confirm password
      | new password     | 12345 |
      | confirm password | 12345 |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    Then User must get logged in and UserName should be displayed in the header

@DevOrder
  Scenario: Verify user is able to login on checkout page by selecting Already registered radio button for a delivery order
     Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user clicks radio button saying Already registered
    And user enters password
      | password | password |
    And user clicks login In button
    Then User must get logged in and UserName should be displayed in the header

@DevOrder
  Scenario: Verify radio options - Continue As Guest,Register After Checkout,Already Registered while does not appear if user login using Already Registered user

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user clicks radio button saying Already registered
    And user enters password
      | password | password |
    And user clicks login In button
    Then radio options - Continue As Guest,Register After Checkout,Already Registered should disappear

  ###############Pickup - checkout#########################################

  Scenario: Verify user can register just after placing Pickup order by clicking Register after checkout
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info along with new emailID
      | customer name  | Shaurya Nigam |
      | customer phone |    3457689024 |
    And user clicks radio button saying Register after Checkout
    And user enters password & confirm password
      | new password     | 12345 |
      | confirm password | 12345 |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    Then User must get logged in and UserName should be displayed in the header
    
@DevOrder
  Scenario: Verify user is able to login on checkout page by selecting Already registered radio button for a ppickup order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user clicks radio button saying Already registered
    And user enters password
      | password | password |
    And user clicks login In button
    Then User must get logged in and UserName should be displayed in the header
    

  Scenario: Verify radio options - Continue As Guest,Register After Checkout,Already Registered does not appear if user login using Already Registered user

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user clicks radio button saying Already registered
    And user enters password
      | password | password |
    And user clicks login In button
    Then radio options - Continue As Guest,Register After Checkout,Already Registered should disappear


  Scenario: Verify navagating away from cart and then coming back to the cart the button should say Checkout
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user navigates back
    And user goes to the cart
    Then The button in the cart says Checkout


  Scenario: Verify cart does not get cleared when user Navigates back from Location tab provided user has added item to the cart

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Locations tab
    And user goes to the cart again
    Then product should be present


  Scenario: Verify stores not yet launched shows message -Coming Soon!
    Given user clicks on Order button
    When user enters the address
      | address | 545 South Hoghland Avenue, Pittsburgh, PA |
    Then Stores not yet launched should displayed message "Coming Soon!"
    

  Scenario: Verify user is able to request notification on Email for store not yet launched
    Given user clicks on Order button
    When user enters the address
      | address | 545 South Hoghland Avenue, Pittsburgh, PA |
    And user clicks on Request Notification
    And user enters emailID to receive notification
      | emailID | snigam@judge.com |
    And user clicks submit
    Then popup should appear saying "You will be sent a notification when this store opens back up!"


  Scenario: Verify user is able to request notification on Phone for stores not yet launched
    Given user clicks on Order button
    When user enters the address
      | address | 545 South Hoghland Avenue, Pittsburgh, PA |
    And user clicks on Request Notification
    And user selects radio button : phone to request notification
    And user enters phone number
      | phone | 9876543210 |
    And user clicks submit
    Then popup should appear saying "You will be sent a notification when this store opens back up!"

@DevOrder
  Scenario: Verify user is not able to submit request notification with wrong emailID format if he enters invalid email format then pop up should appear to enter correct email

    Given user clicks on Order button
    When user enters the address
      | address | 545 South Hoghland Avenue, Pittsburgh, PA |
    And user clicks on Request Notification
    And user enters emailID to receive notification
      | emailID | snigam@judge |
    And user clicks submit
    Then popup should appear saying Please enter a valid email in the text box


  Scenario: Verify request notification phone textbox should not accept less than 10 digits
    Given user clicks on Order button
    When user enters the address
      | address | 545 South Hoghland Avenue, Pittsburgh, PA |
    And user clicks on Request Notification
    And user selects radio button : phone to request notification
    And user enters phone number
      | phone | 98765432 |
    And user clicks submit
    Then popup should appear saying Please enter a phone number in a standard format with the 3-digit area code such as xxx-xxx-xxxx


  Scenario: Verify that when user is selecting delivery button, user should see Address search box for delivery address in cart
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then user should able to see search box option in cart for delivery

  Scenario: TC170 - Verify that user is able to search for delivery address in cart
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user goes to address search box in the cart
    And user enters the address in the cart
      | new delivery address | 513 Frank Thomas Ave, Tuscaloosa, AL 35401, USA |
    Then popup should appear saying - Address updated


  Scenario: TC171 - Verify that user should get a pop up - Order is scheduled for pickup, when selecting pickup button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on cart Pickup button
    Then popup should appear stating - Order is scheduled for pickup

  Scenario: TC172 - Verify when product is added ot the Cart, the text currently displayed in the existing �Order� button will be changed to display �Cart�

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    Then order button should change to Cart button


  Scenario: TC173 - Verify when user selects delivery option on order page and move to cart then delivery option should be selected in cart also

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then Delivery button should be highlighed in purple color


  Scenario: TC174 - Verify when user selects pickup option on order page and move to cart then pickup option should be selected in cart also

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Pickup time & date displayed on the cart top
    Then Pickup button should be highlighed in purple color


  Scenario: TC175 - Verify when user selects pickup option on order page and changes to delivery option in cart by giving different address than a pop should appear stating -This address isn't in this store's delivery zone. Please enter a new address if the entered address in not in delivery zone

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Pickup time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user goes to address search box in the cart
    And user enters the address in the cart
      | new delivery address | 1084 Lancaster Avenue, Bryn Mawr, PA |
    Then popup appears saying - This address isnt in this store's delivery zone. Please enter a new address


  Scenario: TC176 - Verify when user selects delivery option on order page and changes  store name in cart for delivery than a pop up should appear stating -This address isnt in this store's delivery zone if the entered address in not in delivery zone

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user goes to address search box in the cart
    And user enters the address in the cart
      | new delivery address | 1084 Lancaster Avenue, Bryn Mawr, PA |
    Then popup appears saying - This address isnt in this store's delivery zone. Please enter a new address


  Scenario: TC177 - Verify  when user changes option from delivery to pickup
    the address box for delivery should be removed

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    Then Address box for delivery should be removed


  Scenario: TC178 - Verify when user deletes all the items from the cart
    then the Cart button should change to Order button to allow users to order again.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user deletes the items from the cart
    Then Cart button should change to Order button


  Scenario: TC193 - Verify that when user changes from delivery order to pickup in cart then pickup page details
    like Full Name, Email and Phone textbox should appear on checkout page.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    Then checkout page should display details as per pickup order


  Scenario: TC194 - Verify that the shopping cart image display inside the Cart button when product added to cart
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    Then cart image should be displayed inside Cart button

  Scenario: TC195- Verify the address after changing order from pickup to delivery
    in cart and clicking on checkout button
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity should reflect in the cart


  Scenario: TC196- Verify the address remains same after changing order from pickup to delivery
    in cart and clicking on checkout button

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Delivery address should be displayed as mentioned below:
      | Delivery address | Tuscaloosa, AL |

  Scenario: TC198- Verify the address remains same after changing order from delivery to pickup
    in cart and clicking on checkout button

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Pickup address should be displayed as mentioned below:
      | Delivery address | Tuscaloosa, AL |


  Scenario: TC199- Verify user is able to see the delivery and pickup option in Cart
    after clicking on edit option present in cart to edit the pickup/delivery date and time.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then Pickup and Delivery buttons should be present in the cart


  Scenario: TC200- Verify when user Clicks on cart button then, Pickup/delivery option should not be present
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    Then Pickup and Delivery buttons should not be displayed in the cart
    

  Scenario: TC201- Verify when user selects store which has only pickup option on order page than in cart only pickup option should be present

    Given user clicks on Order button
    When user enters the address
      | address | 1319 Anderson Ave |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click The Sugar Rush
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then Only Pickup buttons should be present in the cart


  Scenario: TC204- Verify if user changes order from delivery to pickup in cart than after placing order when user tracks the order it should show pickup tracking information

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as cash
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Delivery address should be displayed on tracking page as mentioned below:
      | Delivery address | Tuscaloosa, AL |

@DevOrder
  Scenario: TC205_1- Verify when user changes order from pickup to delivery and vice versa should get updated on clicking the updated button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks Update button
    Then Delivery should be displayed on the cart top


  Scenario: TC205_2- Verify when user changes order from delivery to pickup and vice versa should get updated on clicking the updated button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks on Pickup button in the cart
    Then Pickup should be displayed on the cart top


  Scenario: TC206 - Verify that when user changes from pickup to delivery order in cart, then delivery page details should appear on checkout page.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    Then checkout page should display details as per delivery order

@DevOrder
  Scenario: TC207 - Verify that user should get a pop up - Order is scheduled for delivery, when selecting delivery button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    Then popup should appear stating - Order is scheduled for delivery
