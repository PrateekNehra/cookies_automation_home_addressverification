@Address3
Feature: Order

  Background: user access to website
    
  
  #| url | https://5.stage.insomniacookies.com/ |

  Scenario Outline: Enter address and verify Delivery option is present and if present take screenshot
  	Given User opens the Website1
    Given user clicks on Order button
    When user enters the Address as "<address>"
    Then Verify if Delivery button is present and get store name if delivery button is present
    
Examples:
| address |
|2311 East Avenue,Columbus,OH,43202|
|1716 Teasley Lane,Denton,TX,76205|
|2626 South Tunnel Boulevard,Pittsburgh,PA,15203|
|1500 Pauline Boulevard,Ann Arbor,MI,48103|
| College View Road,San Marcos,TX,78666|
|1025 South Main Street,Mount Pleasant,MI,48858|
|602 Martin Luther King Junior Boulevard,Chapel Hill,NC,27514|
|1 Eldert Street,New Haven,CT,06511|
|Holland Hall,Greensboro,NC,27401|
|631 Moore Street,San Marcos,TX,78666|
|148 Interstate 45,Huntsville,TX,77340|
|103 East Lincoln Street,Normal,IL,61761|
|3205 Main Drive,Fort Worth,TX,76129|
|2537 Pine Shadows Drive,Huntsville,TX,77320|
|28  Montrose Avenue,Athens,OH,45701|
|520 Spring Street,Lexington,KY,40508|
|402 North 10th Street,Columbia,MO,65201|
|1147 Kater Street,Philadelphia,PA,19147|
|858 20th Street,Knoxville,TN,37916|
|36  Willington Hill Road,Mansfield,CT,06268|
|Annie Boyd Hall, ,Baton Rouge,LA,70802|
|101 Locksley Way,Starkville,MS,39759|
|8  West Canal Street,Richmond,VA,23220|
|1009 Chicago Avenue,Harrisonburg,VA,22802|
|123 College Place,Ypsilanti,MI,48197|
|103 Pressley Drive,Clemson,SC,29631|
|112 Union Street,Providence,RI,02903|
|1540 Spring Garden Street,Greensboro,NC,27403|
|1919 11th Avenue South,Birmingham,AL,35205|
|1939 Heritage Grove Circle,Tallahassee,FL,32304|
|2050  Frederick Douglass Boulevard,New York,NY,10026|
|1312 Poplar Avenue,Indiana,PA,15701|
|1207 South 4th Street,Philadelphia,PA,19147|
|209 West Brody Road,East Lansing,MI,48825|
|Herbert Street,Starkville,MS,39759|
|106 Collegiate Court,Blacksburg,VA,|
|28 Wolfe Street,Athens,OH,45701|
|675 West Jefferson Street,Tallahassee,FL,32304|
|Scotland House, 4701 Elkhorn Avenue,Norfolk,VA,23529|
|219 Wilson Road,East Lansing,MI,48825|
|951 Moore Street,San Marcos,TX,78666|
|945 Learning Way,Tallahassee,FL,32304|
|1750 Euclid Avenue,Cleveland,OH,44115|
|265 East Northwood Avenue,Columbus,OH,43201|
|300 Longwood Avenue,Boston,MA,02115|
|25 Broad Street,New York,NY,10004|
|1804 South Whitcomb Street,Fort Collins,CO,80526|
|631  Moore Street,San Marcos,TX,78666|
|McWhorter Hall,Athens,GA,30605|
|4104 Mercury Drive Southeast,Albuquerque,NM,87116|
|4400 Vestal Parkway East,Binghamton,NY,13902|
|500 East Houston Street,New York,NY,10009|
|312 College Avenue,Ithaca,NY,14850|
|2125 North High Street,Columbus,OH,43210|
|145 Crim Street,Bowling Green,OH,43402|
|3313 Highland Road,Baton Rouge,LA,70802|
|1301 Hillsborough Street,Raleigh,NC,27605|
|549  Ash Street,Lexington,KY,40508|
|1100 Hargrove Road East,Tuscaloosa,AL,35401|
|202 Pine Street Northwest,Atlanta,GA,30313|
|389 Edgewood Avenue,New Haven,CT,06511|
|405 East Vine Street,Oxford,OH,45056|
|926 McMillan Street Northwest,Atlanta,GA,30318|
|Witherspoon Hall, 9515 Poplar Terrace Drive,Charlotte,NC,28223|
|401 South Locust Street,Denton,TX,76201|
|Northern Suites,Indiana,PA,15705|
|214 Owatonna Street,Mankato,MN,56001|
|2405  Anderson Road,Oxford,MS,38655|
|1001 Ocala Road,Tallahassee,FL,32304|
|1721 Jacquelin Street,Richmond,VA,23220|
|1000 Irving Avenue,Syracuse,NY,13210|
|425 West 5th Street,Charlotte,NC,28202|
|3141 Ryan Avenue,Fort Worth,TX,76110|
|606 East Redbud Drive,Stillwater,OK,74075|
|2222 South Crawford Road,Mount Pleasant,MI,48858|
|1102 Mount Royal Drive,Kalamazoo,MI,49009|
|816 East Delmar Street,Springfield,MO,65807|
|311 McDavid Residence Hall South 6th Street,Columbia,MO,65201|
|800 Hitt Street,Columbia,MO,65201|
|1323 Greenwood Avenue,Kalamazoo,MI,49006|
|2501 North Blackwelder Avenue,Oklahoma City,OK,73106|
|6 Butterfield Road,South Kingstown,RI,02881|
|1736 North Sydenham Street,Philadelphia,PA,19121|
|1480 South Rural Road,Tempe,AZ,85281|
|Respect Residence Hall,Columbia,MO,65201|
|6051 U.S. 49,Hattiesburg,MS,39401|
|1801 1st Terrace,Hattiesburg,MS,39401|
|Granville Towers South Building, ,Chapel Hill,NC,27514|
|1410 Nottingham Street,Huntsville,TX,77340|
|61 Chittenden Avenue,Columbus,OH,43201|
|200 Blue Gable Road,Hattiesburg,MS,39401|
|4301 Lincoln Swing Street,Ames,IA,50014|
|Studebaker East Complex,Muncie,IN,47303|
|1722 Sherwood Street,Greensboro,NC,27403|
|439 East Main Street,Kent,OH,44240|
|2722 Pine Street,Boulder,CO,80302|
|1103 West Marshall Street,Richmond,VA,23284|
|1631 19th Street,Boulder,CO,80302|
|975 Lester A Lefton Esplanade,Kent,OH,44243|
|212 Beyer Court,Ames,IA,50012|
| Towson Way,Towson,MD,21204|
|628 East Drachman Street,Tucson,AZ,85705|
|1002 Peterson Street,Fort Collins,CO,80524|
|706 South Division Street,Ann Arbor,MI,48104|
|4266 Spoleto Circle,Oviedo,FL,32765|
|201  Georgia Avenue,Bowling Green,OH,43402|
|2058 Tecumseh Road,Manhattan,KS,66502|
|1920 Exchange Drive,Greenville,NC,27858|
|450 Oklahoma Street,Baton Rouge,LA,70802|
|2629 24th Street,Lubbock,TX,79410|
|120 15th Street East,Tuscaloosa,AL,35401|
|4331 Mercier Street,Kansas City,MO,64111|
|111 East 18th Street,Norfolk,VA,23517|
|810 Saint Vincents Drive,Birmingham,AL,35205|
|505 West Ormsby Avenue,Louisville,KY,40203|
|310 South Limestone,Lexington,KY,40508|
|1434 18th Street South,Birmingham,AL,35205|
|417  North Comanche Street,San Marcos,TX,78666|
|103 Butler Street,Clemson,SC,29631|
|509 West 46th Street,Kansas City,MO,64112|
|602 South College Avenue,Columbia,MO,65201|
|134 Green Lane,Philadelphia,PA,19127|
|626 Marine Street,Boulder,CO,80302|
|900 East Park Street,Carbondale,IL,62901|
|612 Walton Avenue,St. Louis,MO,63108|
|120 North Avenue Northwest,Atlanta,GA,30313|
|3014 North Cramer Street,Milwaukee,WI,53211|
|2376 Alumni Drive,Mansfield,CT,06269|
|1300 Dallas Drive,Denton,TX,76205|
|1011 North Tyndall Avenue,Tucson,AZ,85719|
|2509 Scripture Street,Denton,TX,76201|
|810  Saint Vincents Drive,Birmingham,AL,35205|
|1048 Lois Lane,Harrisonburg,VA,22801|
|975  South Rural Road,Tempe,AZ,85281|
|2337 Highland Avenue,Knoxville,TN,37916|
|306 Kimball Avenue,Hattiesburg,MS,39401|
|226 Bausman Street,Pittsburgh,PA,15210|
|106 Gray Drive,Greensboro,NC,27412|
|1408 Teasley Lane,Denton,TX,76205|
|514 Admiral Way,Manhattan,KS,66502|
|112 East College Avenue,State College,PA,16801|
|1601  North McKinley Avenue,Muncie,IN,47304|
|512 South 3rd Street,Champaign,IL,61820|
|1824 Burns Avenue,Ypsilanti,MI,48197|
|129 Park Lane,San Marcos,TX,78666|
|1 Hospital Drive,Columbia,MO,65201|
|1410  Nottingham Street,Huntsville,TX,77340|
|405 Willard Drive Southeast,Blacksburg,VA,24060|
|833 17th Street,Boulder,CO,80302|
|179 Private Road 3088,Oxford,MS,38655|
|2139 Auburn Avenue,Cincinnati,OH,45219|
|1307 East Sunset Street,Springfield,MO,65804|
|2100 Stantonsburg Road,Greenville,NC,27834|
|106 Grovecrest Way,Greensboro,NC,27406|
|2609 Forest Park Boulevard,Fort Worth,TX,76110|
|944 Marine Street,Boulder,CO,80302|
|560 Buckler Drive,Charlottesville,VA,22903|
|713 Park Point Drive,Rochester,NY,14623|
|3484 Vine Street,Cincinnati,OH,45220|
|Linkins Dining Center North Adelaide Street,Normal,IL,61761|
|6210 Abbot Road,East Lansing,MI,48823|
|San Marcos Hall Llano Circle,San Marcos,TX,78666|
|270 Akron General Avenue,Akron,OH,44307|
|3320 Cliff Road,Birmingham,AL,35205|
|913 Garver Street,Norman,OK,73069|
|301 10th Street Northwest,Atlanta,GA,30318|
|145 Hackberry Lane,Tuscaloosa,AL,35401|
|2101 Harrison Avenue,Fort Worth,TX,76110|
|5230 Centre Avenue,Pittsburgh,PA,15232|
|1301  Aquarena Springs Drive,San Marcos,TX,78666|
|6405 North Drive,University City,MO,63130|
|2447 Brownlee Street,Baton Rouge,LA,70808|
|1001 East Harrison Street,Springfield,MO,65807|
|3615 19th Street,Lubbock,TX,79410|
|120 West Woodruff Avenue,Columbus,OH,43210|
|1900 Barkley Street,Norman,OK,73071|
|1912 University Avenue,Morgantown,WV,26505|
|2011 Grand Avenue,Knoxville,TN,37916|
|916 West 10th Avenue,Stillwater,OK,74074|
|100 East Lindsey Street,Norman,OK,73072|
|2141 Baseline Road,Boulder,CO,80302|
|Vista Del Sol D,Tempe,AZ,85281|
|1006 Chapel Street,New Haven,CT,06510|
|2233 Arapahoe Avenue,Boulder,CO,80302|
|Fraternity Road,Rochester,NY,14611|
|8 Foster Drive,Norman,OK,73071|
|1590 Access Road,Oxford,MS,38655|
|129  South Ithan Avenue,Bryn Mawr,PA,19010|
|800  East Grand Avenue,Carbondale,IL,62901|
|1001 14th Street South,Birmingham,AL,35205|
|113 Chesterfield Rd,Pittsburgh,PA,|
|Stradley Hall,Columbus,OH,43210|
|103 College Heights Boulevard,Clemson,SC,29631|
|1213  North Madison Street,Bloomington,IL,61701|
|1819 Panhandle Street,Denton,TX,76201|
|1309 East Hennepin Avenue,Minneapolis,MN,55414|
|1710 West 4th Avenue,Stillwater,OK,74074|
|1415 North Manning Street,Stillwater,OK,74075|
|Sigma Chi, Fraternity, 3 Fraternity Way,St. Louis,MO,63105|
|3733 Lindell Boulevard,St. Louis,MO,63108|
|407 East Willow Street,Normal,IL,61761|
| East Winter Park Drive,Stillwater,OK,74075|
|2312 20th Street,Lubbock,TX,79411|
|1150 River Ridge Parkway,San Marcos,TX,78666|
|Vel Phillips Hall, 1950 Willow Drive,Madison,WI,53706|
|2000 Oak Grove Rd,Hattiesburg,MS,|
|120 15th Street East,Tuscaloosa,AL,35401|
|207 South Bonnie Brae Street,Denton,TX,76201|
|Moore Hall, 1401 Claflin Road,Manhattan,KS,66502|
|712 South McCann Avenue,Springfield,MO,65804|
|710 East Boyd Drive,Baton Rouge,LA,70808|
|1136 5th Avenue South,Fargo,ND,58103|
|2395 East Wesley Avenue,Denver,CO,80210|
|1822 North Perkins Road,Stillwater,OK,74075|
|4301 Madison Avenue,Kansas City,MO,64111|
|Colby Residence Hall,Fort Worth,TX,76109|
|3443 South Hills Avenue,Fort Worth,TX,76109|
|405 Foxfire Drive,Columbia,MO,65201|
|3720 Laclede Avenue,St. Louis,MO,63108|
|1300 Rollins Street,Columbia,MO,65201|
|1246 Bardwell Road,Starkville,MS,39759|
|105  Doleac Drive,Hattiesburg,MS,39401|
|4029 Rhodes Avenue,Memphis,TN,38111|
|201 North Shartel Avenue,Oklahoma City,OK,73102|
|519  South Meldrum Street,Fort Collins,CO,80526|
|820  Emerald Street Southeast,Saint Paul,MN,55114|
|1900 South 1st Street,Champaign,IL,61820|
|650 River Road,San Marcos,TX,78666|
|3615 19th Street,Lubbock,TX,79410|
|2101 Sycamore Avenue,Huntsville,TX,77340|
|1412 East Gateway Circle South,Fargo,ND,58103|
|1209 Searle Drive,Normal,IL,61761|
|1 Grand Avenue,San Luis Obispo,CA,93405|
|1721 Hutchison Drive,Davis,CA,95616|
|1111 Avalon Avenue,San Marcos,TX,78666|
|654 Abercrombie Lane,Tuscaloosa,AL,35401|
|2311 Stella Street,Denton,TX,76201|
|791 East 15th Avenue,Eugene,OR,97401|
|1975 Aquarena Springs Drive,San Marcos,TX,78666|
|310 South 16th Avenue,Hattiesburg,MS,39401|
|821 East Walnut Street,Columbia,MO,65201|
|4117 University Avenue,Cedar Falls,IA,50613|
|2712 22nd Street,Lubbock,TX,79410|
|622 East Myrtle Street,Fort Collins,CO,80524|
|1044 West Imhoff Road,Norman,OK,73072|
|516 High Street,Bellingham,WA,98225|
|1 Grand Avenue,San Luis Obispo,CA,93407|
|1075 Oak Street,Reno,NV,89503|
|200  North Santa Rosa Street,San Luis Obispo,CA,93405|
|1  Grand Avenue,San Luis Obispo,CA,93405|
      
    