@Address1
Feature: Order

  Background: user access to website
    
  
  #| url | https://5.stage.insomniacookies.com/ |

  Scenario Outline: Enter address and verify Delivery option is present and if present take screenshot
  	Given User opens the Website1
    Given user clicks on Order button
    When user enters the Address as "<address>"
    Then Verify if Delivery button is present and get store name if delivery button is present
    
    Examples:
| address |
|150 Charles Street,New York,NY,10014|
|2482 Paris Street,Cincinnati,OH,45219|
|612 West Adams Street,Muncie,IN,47305|
|4000 Spruce Street,Philadelphia,PA,19104|
|1311 Cumberland Avenue,Knoxville,TN,37916|
|543 Church Street,Ann Arbor,MI,48104|
|914 Douglas Street,Mount Pleasant,MI,48858|
|5 Knight Lane,Trenton,NJ,08638|
|2007 West Cary Street,Richmond,VA,23220|
|1310 West 39th Street,Norfolk,VA,23508|
|3200 North Alafaya Trail,Orlando,FL,32826|
|800 Ardmore Avenue,Ardmore,PA,19003|
|12 Collegiate Way,Mount Pleasant,MI,48858|
|1606 Walker Avenue,Greensboro,NC,27403|
|153 Commonwealth Avenue,Amherst,MA,01002|
|150 Union Street,Providence,RI,02903|
|Forbes-Craig Honors House, 4531 Forbes Avenue,Pittsburgh,PA,15213|
|32 Tremont Avenue,Binghamton,NY,13903|
|407 South Elm Street,Greenville,NC,27858|
|1840 North 17th Street,Philadelphia,PA,19121|
|917 Tower Court Road,Mansfield,CT,06268|
|Fay House, 10 Garden Street,Cambridge,MA,02138|
|1794 Poplar Grove Road South,Boone,NC,28607|
|Village C East,Washington,DC,20007|
|75 Bowery,New York,NY,10002|
|235 South Ocala Road,Tallahassee,FL,32304|
|601 Children's Lane,Norfolk,VA,23507|
|400 Hope Street,Providence,RI,02906|
|3901 Locust Walk,Philadelphia,PA,19104|
|1311 Lake Loudoun Boulevard,Knoxville,TN,37916|
|374 Allyn Street,Akron,OH,44304|
|4945 Park Avenue,Wilmington,NC,28403|
|530 Washington Street Southwest,Blacksburg,VA,24060|
|1035 Red Mile Road,Lexington,KY,40504|
|700 West Broad Street,Richmond,VA,23220|
|630 Pickens Street,Columbia,SC,29201|
|8540 Digital Drive,Charlotte,NC,28262|
|Collegiate Way,Union charter Township,MI,48858|
|1603 North McKinley Avenue,Muncie,IN,47304|
|615 Saluda Avenue,Columbia,SC,29205|
|20 York Street,New Haven,CT,06510|
|95 8th Street Northwest,Atlanta,GA,30308|
|4701 North Charles Street,Baltimore,MD,21210|
|3960 Bostic Drive,Greenville,NC,27834|
|Kenan Residence Hall, 112 Battle Lane,Chapel Hill,NC,27514|
|355 Wilson Avenue,Morgantown,WV,26501|
|2489 San Miguel Avenue,Tallahassee,FL,32304|
|2124 Sidney Street,Pittsburgh,PA,15203|
|1601 The Edge North 15th Street,Philadelphia,PA,19121|
|238 West Genesee Street,Syracuse,NY,13202|
|231 East 10th Street,New York,NY,10003|
|870  Congress Avenue,New Haven,CT,06519|
|3400  Spruce Street,Philadelphia,PA,19103|
|522 Thurstin Street,Bowling Green,OH,43403|
|54 Elm Street,Clemson,SC,29631|
|1547  North Hagadorn Road,East Lansing,MI,48823|
|3333 Fifth Avenue,Pittsburgh,PA,15213|
|100  Wyndham Circle,Greenville,NC,27858|
|535 East 86th Street,New York,NY,10028|
|811 West Carolina Street,Tallahassee,FL,32304|
|201 Wood Street,Pittsburgh,PA,15222|
|2016  Hillside Road,Mansfield,CT,06269|
|1110 Maiden Lane Court,Ann Arbor,MI,48105|
|201 Stone Mill Run,Athens,GA,30605|
|501  East 2nd Street,Greenville,NC,27858|
|1234 State Route 2004,Philadelphia,PA,19107|
|7 Ronan Street,Binghamton,NY,13905|
|2700 University Court,Cincinnati,OH,45219|
|500 Martha Jefferson Drive,Charlottesville,VA,22911|
|1008 South Main Street,Blacksburg,VA,24060|
|1730 Bank Street,Baltimore,MD,21231|
|8300 Baltimore Avenue,College Park,MD,20740|
|45 West Daniels Street,Cincinnati,OH,45219|
|Ivy Ct,East Lansing,MI,48823|
|134 Mensa Lane,Oviedo,FL,32765|
|706 Warren Street,Greensboro,NC,27403|
|1596 East Summit Street,Kent,OH,44240|
|55 East 10th Street,New York,NY,10003|
|James S. White Hall, 2108 North Broad Street,Philadelphia,PA,19121|
|301 East 47th Street,New York,NY,10017|
|627 West 115th Street,New York,NY,10025|
|Center Hall, 2108 Unity Place,Louisville,KY,40208|
|Granville Towers East,Chapel Hill,NC,27516|
|129  South Ithan Avenue,Bryn Mawr,PA,19010|
|1064 Varsity Lane,Charlotte,NC,28262|
|Merrow Hall, ,South Kingstown,RI,02881|
|20 Exchange Place,New York,NY,10005|
|Creswell Hall,Athens,GA,30605|
|1307 Cedarwood Drive,Kent,OH,44240|
|1201 Market Street,Philadelphia,PA,19107|
|27 East 7th Street,New York,NY,10003|
|48 Butterfield Road,South Kingstown,RI,02881|
|1149 Olympia Avenue,Columbia,SC,29201|
|241 Elm Street,New Haven,CT,06511|
|700 North Woodward Avenue,Tallahassee,FL,32304|
|188 South College Street,Akron,OH,44304|
|314  South Drake Road,Kalamazoo,MI,49009|
|161 Orchard Hill Drive,Amherst,MA,01003|
|23 Acorn Circle,Towson,MD,21286|
|3333 Fifth Avenue,Pittsburgh,PA,15213|
|,undefined,und,undefined|
|1000 South Limestone,Lexington,KY,40536|
|101 South Woodward Avenue,Tallahassee,FL,32304|
|700 North Woodward Avenue,Tallahassee,FL,32304|
|705  South 17th Street,Knoxville,TN,37916|
|1160 West Stadium Avenue,West Lafayette,IN,47906|
|229 East King Street,Boone,NC,28607|
|400 West 119th Street,New York,NY,10027|
|409 Edgehill Drive,Oxford,OH,45056|
|2141 Silver Maple Lane,Greenville,NC,27858|
|1500 East Medical Center Drive,Ann Arbor,MI,48109|
|227 Hagans Street,Morgantown,WV,26501|
|2313 Wrightsville Avenue,Wilmington,NC,28403|
|Enchanted Lane,Charlotte,NC,28213|
|220 South Depeyster Street,Kent,OH,44240|
|173  Culbreth Road,Charlottesville,VA,22903|
|562 East High Street,Lexington,KY,40502|
|1100 Chancellor Park Drive,Charlotte,NC,28213|
|1119 Donington Circle,Towson,MD,21204|
|301 Lippencott Street,Knoxville,TN,37920|
|263 Taylor Street,Morgantown,WV,26505|
|1719 Willington Street,Philadelphia,PA,19121|
|9 Hancock Drive,Charlottesville,VA,22903|
|34 Allen Street,Johnson City,NY,13790|
|Bryan Circle,Clemson,SC,29631|
|1030  Blue Ridge Drive,Harrisonburg,VA,22802|
|2453 Talco Hills Drive,Tallahassee,FL,32303|
|728 Garden Street,Hoboken,NJ,07030|
|515 Hinton James Drive,Chapel Hill,NC,27514|
|328 Stoney Creek Lane,Clemson,SC,29631|
|3600 West School House Lane,Philadelphia,PA,19129|
|1400 Locust Street,Pittsburgh,PA,15219|
|Goldsworth Valley #2,Kalamazoo,MI,49006|
|212 Ascot Lane,Blacksburg,VA,24060|
|111 Harmon Street,Charlottesville,VA,22903|
|3990 Fifth Avenue,Pittsburgh,PA,15213|
|1709 Walker Avenue,Greensboro,NC,27403|
|405 Garrett Street,Charlottesville,VA,22902|
|1836  Cumberland Avenue,Knoxville,TN,37916|
|2243  Chester Avenue,Cleveland,OH,44114|
|811 North Drive,Blacksburg,VA,24060|
|,undefined,und,undefined|
|609 South Lincoln Street,Kent,OH,44240|
|2004 18th Street Northwest,Washington,DC,20009|
|750 West Hampton Drive,Indianapolis,IN,46208|
|3435 Saddle Boulevard,Orlando,FL,32826|
|1925  Ring Road North,Kalamazoo,MI,49006|
|155 Temple Street,New Haven,CT,06510|
|1 Park Street,New Haven,CT,06510|
|400 North High Street,Columbus,OH,43215|
|2243 Sagamore Parkway West,West Lafayette,IN,47906|
|715 Congress Street,Ypsilanti,MI,48197|
|819 North 8th Street,Terre Haute,IN,47807|
|300 Boston Post Road,West Haven,CT,06516|
|2216 Vine Street,Cincinnati,OH,45219|
|1014 Blue Ridge Drive,Harrisonburg,VA,22802|
|409 Wesley Avenue,Wilmington,NC,28403|
|45  Upper College Road,South Kingstown,RI,02881|
|1037 West 39th Street,Norfolk,VA,23508|
|520 East 6th Street,New York,NY,10009|
|800 Basin Street,Tallahassee,FL,32304|
|207 Arnet Street,Ypsilanti,MI,48198|
|1021 Clinton Street,Philadelphia,PA,19107|
|500 South Greene Street,Greenville,NC,27834|
|Tyler Residence Hall,Greenville,NC,27858|
|500 Houston Street,Blacksburg,VA,24060|
|60 Colony Manor Drive,Rochester,NY,14623|
|Tucker Hall,South Kingstown,RI,02881|
|427 West College Avenue,Tallahassee,FL,32301|
|956  Church Street,Indiana,PA,15701|
|1318 MacArthur Street,Blacksburg,VA,24060|
|1102 West University Avenue,Muncie,IN,47303|
| Curtin Road,State College,PA,16802|
|170 Elizabeth Street,New York,NY,10012|
|1336 Varsity Lane,Charlotte,NC,28262|
| East Circle Drive,Ypsilanti,MI,48197|
|640 Pinecrest Drive,Athens,GA,30605|
|Eddy Hall, ,South Kingstown,RI,02881|
|Bodenheimer Drive,Boone,NC,28607|
|5909 Horning Road,Kent,OH,44240|
|1160 West Stadium Avenue,West Lafayette,IN,47906|
|111 West 29th Street,Baltimore,MD,21218|
|304 York Way,Pittsburgh,PA,15213|
|300 Boston Post Road,West Haven,CT,06516|
|831 North 5th Street,Terre Haute,IN,47807|
|74 3rd Avenue,New York,NY,10003|
|20 South College Street,Athens,OH,45701|
|500 Daniel Drive,Clemson,SC,29631|
|649 East 14th Street,New York,NY,10009|
|1511 Clinch Avenue,Knoxville,TN,37916|
|1038 20th Street,Knoxville,TN,37916|
|5810  Copper Beech Boulevard,Kalamazoo,MI,49009|
|1216 South Broad Street,Philadelphia,PA,19146|
|137 Sumac Street,Philadelphia,PA,19128|
|3724 Libra Drive,Orlando,FL,32826|
|Bates House,Columbia,SC,29205|
|1820 Heritage Pond Drive,Charlotte,NC,28262|
|817 East Shaw Lane,East Lansing,MI,48825|
|210 Dan Allen Drive,Raleigh,NC,27606|
| East 5th Street,Greenville,NC,27858|
|315 North 12th Street,Philadelphia,PA,19107|
|101 Lake Avenue,Orlando,FL,32801|
|Geer Hall,Clemson,SC,29634|
|113 Yorkshire Court,Blacksburg,VA,24060|
|1361 MacArthur Street,Blacksburg,VA,24060|
|25 Union Square West,New York,NY,10003|
|421 West Hancock Avenue,Athens,GA,30601|
|Lake Claire Building 64,Orlando,FL,32816|
|816 Castlewood Drive,Greensboro,NC,27405|
|321 East Fairmount Avenue,State College,PA,16801|
|5534 Panorama Ave,Charlotte,NC,28213|
|2 Mount Olympus Drive,Syracuse,NY,13210|
|600 Heyward Street,Columbia,SC,29201|
|22  South Grove Street,Ypsilanti,MI,48198|
|Hanks Hall,Mansfield,CT,06269|
|103 Burley Avenue,Lexington,KY,40503|
|1409 West Jefferson Street,Philadelphia,PA,19121|
|121 College Heights Boulevard,Clemson,SC,29631|
|2524 Sidney Street,Pittsburgh,PA,15203|
|704 Westcott Street,Syracuse,NY,13210|
|443 Columbia Avenue,Rochester,NY,14611|
|15 East 11th Street,New York,NY,10003|
|2141 Silver Maple Lane,Greenville,NC,27858|
|3901 South 7th Street,Terre Haute,IN,47802|
|2222 Spring Street,Pittsburgh,PA,15210|
|1360 Highland Street,Columbus,OH,43201|
|Simmons Hall,State College,PA,16801|
|Elizabeth Hicks Residence Hall,Mansfield,CT,06269|
|335 Village Drive,East Lansing,MI,48823|
|1519 Pine Street,Philadelphia,PA,19102|
|1800 Orleans Street,Baltimore,MD,21287|
|Park Hall, 120 West 11th Avenue,Columbus,OH,43210|
|805 East 3rd Street,Greenville,NC,27858|
|Seahawk Crossing,Wilmington,NC,28403|
|228 South College Drive,Bowling Green,OH,43402|
|914 South State Street,Ann Arbor,MI,48104|
|4690 Vestal Parkway East,Vestal,NY,13850|
|5262 Brown Road,Oxford,OH,45056|
|23 Elliott Street,Athens,OH,45701|
|111 Jane Street,New York,NY,10014|
|29 Lower College Road,South Kingstown,RI,02881|
|1930  Chestnut Street,Philadelphia,PA,19103|
|319 South Main Street,Oxford,OH,45056|
|499 Northside Circle Northwest,Atlanta,GA,30309|
|400 North River Road,West Lafayette,IN,47906|
|12900 Waterford Wood Circle,Orlando,FL,32828|
|1830 Cumberland Avenue,Knoxville,TN,37916|
|Cumberland Hotel, 1109 White Avenue,Knoxville,TN,37916|
|217 White Drive,Tallahassee,FL,32304|
|103 North Library Street,Greenville,NC,27858|
      
    