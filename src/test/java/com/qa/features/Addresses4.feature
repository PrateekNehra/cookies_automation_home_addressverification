@Address4
Feature: Order

  Background: user access to website
    
  
  #| url | https://5.stage.insomniacookies.com/ |

  Scenario Outline: Enter address and verify Delivery option is present and if present take screenshot
  	Given User opens the Website1
    Given user clicks on Order button
    When user enters the Address as "<address>"
    Then Verify if Delivery button is present and get store name if delivery button is present
    
    Examples:
| address |
|1003 28th Place South,Birmingham,AL,35205|
|502 North Frances Street,Madison,WI,53703|
|68 Chorro Street,San Luis Obispo,CA,93405|
|500 Palisades Drive,Birmingham,AL,35209|
|710 31st Street,Boulder,CO,80303|
|85 Dawg Drive,Starkville,MS,39759|
|1011 North Tyndall Avenue,Tucson,AZ,85719|
|415 North Chorro Street,San Luis Obispo,CA,93405|
|900 2nd Street,Tuscaloosa,AL,35401|
|3445 Hensley Street,Reno,NV,89503|
|1710 East 15th Avenue,Eugene,OR,97403|
|1990 Loomis Street,San Luis Obispo,CA,93405|
|2255 Sycamore Lane,Davis,CA,95616|
|1455  Northeast Brandi Way,Pullman,WA,99163|
|Kennedy Library, ,San Luis Obispo,CA,93405|
|977 East Apache Boulevard,Tempe,AZ,85281|
|1000 Patterson Street,Eugene,OR,97401|
|703 Riverside Lane,Tuscaloosa,AL,35401|
|North Mountain Dorms, 1 Grand Avenue,San Luis Obispo,CA,93405|
|1511 Fetters Loop,Eugene,OR,97402|
|1 Grand Avenue,San Luis Obispo,CA,93405|
|1365 Agate Street,Eugene,OR,97403|
|730 32nd Street,Boulder,CO,80303|
|159 Orange Drive,San Luis Obispo,CA,93405|
|Aliso, ,San Luis Obispo,CA,93405|
|500 Northeast Maiden Lane,Pullman,WA,99163|
|809 West Nevada Street,Urbana,IL,61801|
|1055 Northeast B Street,Pullman,WA,99163|
| Unnamed Road,San Marcos,TX,78666|
|1339 Marsh Street,San Luis Obispo,CA,93401|
|1802 6th Avenue South,Birmingham,AL,35233|
|250 California Boulevard,San Luis Obispo,CA,93405|
|1340 Humboldt Street,Bellingham,WA,98225|
|258 California Boulevard,San Luis Obispo,CA,93405|
|1440 Brazos Drive,Huntsville,TX,77320|
|Northeast Ruby Street,Pullman,WA,99163|
|460 Marion Road,Oshkosh,WI,54901|
|2911 Rollins Road,Columbia,MO,65203|
|417 South Locust Street,Denton,TX,76201|
|301 Santa Maria Avenue,San Luis Obispo,CA,93405|
|313 Columbia Drive Southeast,Albuquerque,NM,87106|
|Living Learning Center North, ,Eugene,OR,97403|
|846 College Avenue,Norman,OK,73069|
|10 Nottingham Street,Huntsville,TX,77340|
|2413 North Kentucky Avenue,Oklahoma City,OK,73106|
|1007 24th Street,Bellingham,WA,98225|
|115 West 6th Street,Tempe,AZ,85281|
|1965  East 15th Avenue,Eugene,OR,97403|
|2455 Montgomery Road,Huntsville,TX,77340|
|1599 East 15th Avenue,Eugene,OR,97403|
|777 Boysen Avenue,San Luis Obispo,CA,93405|
|791 East 15th Avenue,Eugene,OR,97401|
|1450 Carson Hall East 13th Avenue,Eugene,OR,97403|
|1248 Southeast Latah Street,Pullman,WA,99163|
|2024 Northeast Terre View Drive,Pullman,WA,99163|
|1024 Southeast Latah Street,Pullman,WA,99163|
|203 1st Street,Davis,CA,95616|
|1935 Southeast Nevada Street,Pullman,WA,99163|
|5451 McLeod Drive,Las Vegas,NV,89120|
|1201 North Virginia Street,Reno,NV,89503|
|1230 Murray Avenue,San Luis Obispo,CA,93405|
|751 Elmwood Drive,Davis,CA,95616|
|Centennial Halls, 1870 South High Street,Denver,CO,80210|
| Unnamed Road,Pullman,WA,99163|
|510 West University Drive,Tempe,AZ,85281|
|55 Stenner Street,San Luis Obispo,CA,93405|
|Santa Lucia Hall,San Luis Obispo,CA,93410|
|1475 East 15th Avenue,Eugene,OR,97403|
|1911 Johnson Avenue,San Luis Obispo,CA,93401|
|1010 Murray Avenue,San Luis Obispo,CA,93405|
|348 Hathway Avenue,San Luis Obispo,CA,93405|
|510 East University Drive,Tempe,AZ,85281|
|Canyon Circle,San Luis Obispo,CA,93410|
|1455 Northeast Brandi Way,Pullman,WA,99163|
|455 Northeast Morton Street,Pullman,WA,99163|
|755 Elmwood Drive,Davis,CA,95616|
|205 Ott Road,Pullman,WA,99164|
|1256 South 45th Street,Philadelphia,PA,19104|
|2004 Greenspring Drive,Lutherville-Timonium,MD,21093|
|6614 Shepley Drive,Clayton,MO,63105|
|665 Elm Street,Terre Haute,IN,47807|
|545 Ray C Hunt Drive,Charlottesville,VA,22908|
|134 Lacey Street,West Chester,PA,19382|
|2000 Pennington Road,Ewing Township,NJ,08618|
|200 West Street,New York,NY,10282|
|691 Exchange Street,Rochester,NY,14608|
|555 Fairmount Avenue,Towson,MD,21286|
|Baltimore County Courts Building, 401 Bosley Avenue,Towson,MD,21204|
|1319 2nd Avenue,New York,NY,10021|
|50 West 23rd Street,New York,NY,10010|
|Ross School of Business Building, 701 Tappan Avenue,Ann Arbor,MI,48109|
|1 Hospital Drive,Columbia,MO,65201|
|Miami Commons,Oxford,OH,45056|
|106/108 L B J Cove,San Marcos,TX,78666|
|231 East Chestnut Street,Louisville,KY,40202|
|2139 Auburn Avenue,Cincinnati,OH,45219|
|301 South College Street,Charlotte,NC,28202|
|820 John Street,Kalamazoo,MI,49001|
|120 IRMC Drive,Indiana,PA,15701|
|Hawthorn Residence Hall, 903 Hitt Street,Columbia,MO,65201|
|Greenleaf Court,Rochester,NY,14623|
|3000 West School House Lane,Philadelphia,PA,19144|
|2540 Hale Avenue,Memphis,TN,38112|
|306 Electrical and Computer Engineering Building North Wright Street,Urbana,IL,61801|
|575  River Street,Hoboken,NJ,07030|
|2915 North Classen Boulevard,Oklahoma City,OK,73106|
|Information Sciences and Technology Building, ,State College,PA,16802|
|41 West 96th Street,New York,NY,10025|
|306 Electrical and Computer Engineering Building North Wright Street,Urbana,IL,61801|
|12820 Gemini Boulevard South,Orlando,FL,32816|
|644 Greenwich Street,New York,NY,10014|
|2201 West University Avenue,Muncie,IN,47303|
|950 West Wooster Street,Bowling Green,OH,43402|
|3310 Ruther Avenue,Cincinnati,OH,45220|
|421 Gernert Court,Louisville,KY,40217|
|225 Fifth Avenue,Pittsburgh,PA,15222|
|425 East 67th Street,New York,NY,10065|
|517 Norton Parkway,New Haven,CT,06511|
|1800 Denison Avenue,Manhattan,KS,66506|
|353 East Michigan Avenue,Kalamazoo,MI,49007|
|1100 Peachtree Street Northeast,Atlanta,GA,|
|1 East 33rd Street,New York,NY,10016|
|953 Danby Road,Ithaca,NY,14850|
|340 East Huron Street,Ann Arbor,MI,48104|
|711 East Krayler Avenue,Stillwater,OK,74075|
|200 Madison Avenue,New York,NY,10016|
|6511 North Sheridan Road,Chicago,IL,60626|
|245 5th Avenue,New York,NY,10016|
|Willard Henry Dow Laboratory, 930 North University Avenue,Ann Arbor,MI,48109|
|375 Park Avenue,New York,NY,10022|
|1115 West Nevada Street,Urbana,IL,61801|
|718 West College Avenue,Normal,IL,61761|
|7914 Knollwood Road,Towson,MD,21286|
|440 Park Avenue South,New York,NY,10016|
|300 George Street,New Haven,CT,06511|
|630 West Washington Avenue,Madison,WI,53703|
|825 Northeast 10th Street,Oklahoma City,OK,73104|
|4126 Roanoke Road,Kansas City,MO,64111|
|475 Park Avenue South,New York,NY,10016|
|1551 Lake Loudoun Boulevard,Knoxville,TN,37916|
|21 Northcross Road,Ithaca,NY,14853|
|200 Arnet Street,Ypsilanti,MI,48198|
|MetLife Building, 200 Park Avenue,New York,NY,10166|
|200 Broadway,New York,NY,10038|
|10 North Lee Avenue,Oklahoma City,OK,73102|
|430 Congress Avenue,New Haven,CT,06519|
|200  South Orange Avenue,Orlando,FL,32801|
|4400 Haughey Avenue,Indianapolis,IN,46208|
|530 South Milledge Avenue,Athens,GA,30605|
|1520 York Avenue,New York,NY,10028|
|1011 Gardner Street,Raleigh,NC,27607|
|4921 Parkview Place,St. Louis,MO,63110|
|2927 Devine Street,Columbia,SC,29205|
|281 West Lane Avenue,Columbus,OH,43210|
|201 Abraham Flexner Way,Louisville,KY,40202|
|300 East Lombard Street,Baltimore,MD,|
|4112 4th Avenue South,Birmingham,AL,35222|
|1400 University Avenue,Charlottesville,VA,22903|
|525 East 68th Street,New York,NY,10065|
|800 Market Street,Knoxville,TN,37902|
|33 Whitehall Street,New York,NY,10004|
|3401 Civic Center Boulevard,Philadelphia,PA,19104|
|580 Dixwell Avenue,New Haven,CT,06511|
|289 South Union Street,Akron,OH,44304|
|610 East University Avenue,Ann Arbor,MI,48109|
|Starrett-Lehigh Building, 601 West 26th Street,New York,NY,10001|
|290 King of Prussia Road,Wayne,PA,19087|
|375 Park Avenue,New York,NY,10022|
|1425 East Ann Street,Ann Arbor,MI,48109|
|150  Sawgrass Drive,Rochester,NY,14620|
|985 Pleasant Street,Boulder,CO,80302|
|1924 Alcoa Highway,Knoxville,TN,37920|
|1122 Northeast 13th Street,Oklahoma City,OK,73117|
| Unnamed Road,Columbia,SC,29201|
|240 Student Un,Stillwater,OK,74078|
|318 South College Road,Wilmington,NC,28403|
|728 South Broad Street,Philadelphia,PA,19146|
|1923 Pointe Lane,Ann Arbor,MI,48105|
|257 East 18th Avenue,Columbus,OH,43201|
|180 Sawgrass Drive,Rochester,NY,14620|
|321 South Main Street,Oxford,OH,45056|
|290 Russell Street,Starkville,MS,39759|
|1015 North Washburn Street,Oshkosh,WI,54904|
|Craig Hall, 200 South Craig Street,Pittsburgh,PA,15260|
|40 West 25th Street,New York,NY,10010|
|1090 35th Street North,Fargo,ND,58102|
|106 Northwest 12th Street,Oklahoma City,OK,73103|
|11 Broadway,New York,NY,|
|37 West 72nd Street,New York,NY,10023|
|599 599 Lexington Avenue Lexington Avenue,New York,NY,10022|
|615 Baxter Street,Athens,GA,30605|
|1530 East Wooster Street,Bowling Green,OH,43402|
|1111 East Broad Street,Richmond,VA,23219|
|225  Broadway,New York,NY,10007|
|4400 Vestal Parkway East,Binghamton,NY,13902|
|831 North 5th Street,Terre Haute,IN,47807|
|220 East 42nd Street,New York,NY,10017|
|5100 Prairie Parkway,Cedar Falls,IA,50613|
|2004 Greenspring Drive,Lutherville-Timonium,MD,21093|
|1111  Garden Street,Hoboken,NJ,07030|
|300 Park Avenue South,New York,NY,10010|
|100 Light Street,Baltimore,MD,21202|
|30 East 33rd Street,New York,NY,10016|
|Honors Commons Building, 2221 Dunn Avenue,Raleigh,NC,27607|
|675  West Jefferson Street,Tallahassee,FL,32304|
|4801  Main Street,Kansas City,MO,64112|
|Leebrick Drive,Kent,OH,44243|
|Green Building, 1215 Avenue J,Lubbock,TX,79401|
|818 North Benton Avenue,Springfield,MO,65802|
|650 South Exeter Street,Baltimore,MD,21202|
|4015 22nd Place,Lubbock,TX,79410|
|3333  North Calvert Street,Baltimore,MD,21218|
|220 East 42nd Street,New York,NY,10017|
|1717 Shipyard Boulevard,Wilmington,NC,28403|
|Woodworth Complex,Muncie,IN,47303|
|Eastman Student Living Center Grove Place,Rochester,NY,14605|
|230 Hoey Residence Hall Hardin Street,Boone,NC,28607|
|901 Staton Road,Greenville,NC,27834|
|40 West 4th Street,New York,NY,10012|
|390 North Orange Avenue,Orlando,FL,32801|
|525 East 71st Street,New York,NY,10021|
|1215 Lee Street,Charlottesville,VA,22903|
|1021 East Harrison Street,Springfield,MO,65807|
|500 Park Avenue,New York,NY,10022|
|431 Atwood Street,Pittsburgh,PA,15213|
|611 North Austin Street,Denton,TX,76201|
|1634 Patrick Henry Drive,Blacksburg,VA,24060|
|3401 Civic Center Boulevard,Philadelphia,PA,19104|
|3555 Moser Street,Oshkosh,WI,54901|
|1924 Alcoa Highway,Knoxville,TN,37920|
|7405 Westfield Boulevard,Indianapolis,IN,46240|
|200 East 95th Street,New York,NY,10128|
|181  Fearing Street,Amherst,MA,01003|
|2440 West Henrietta Road,Rochester,NY,14623|
|2000 Olathe Boulevard,Kansas City,KS,66103|
|343  North Grove Street,Bowling Green,OH,43402|
|1000 East Primrose Street,Springfield,MO,65807|
|341 Western College Drive,Oxford,OH,45056|
|101 Manning Drive,Chapel Hill,NC,27514|
|3400 North Charles Street,Baltimore,MD,21218|
|1137 South 24th Street,Philadelphia,PA,19146|
|539 North Orianna Street,Philadelphia,PA,19123|
|401 East 34th Street,New York,NY,10016|
|1981 North McKinley Avenue,Muncie,IN,47304|
|900 Hillsborough Street,Raleigh,NC,27603|
|242 King Avenue,Athens,GA,30606|
|100 Lakeside Avenue East,Cleveland,OH,44114|
|520 Madison Avenue,New York,NY,10022|
|Templeton Athletic Academic Center, ,Starkville,MS,39759|
|101 East Blount Avenue,Knoxville,TN,37920|
      
    