@Tracker

Feature: Tracker - This module is used to track Delivery & Pickup Orders.


Background: user access to website
    Given User opens the Website
    And if user is already logged in- just logout
 
@DevTracker    
    Scenario: TC001 - Verify user is able to access Cookie Tracker Page
    And user clicks Tracker tab
    Then Tracker page should be displayed successfully to the user


    Scenario: TC002 - Verify Placeholder text "Tracker ID" should be displayed in the search textbox
    And user clicks Tracker tab
    Then placeholder text Tracker ID should be displayed in textbox
    
    
    Scenario: TC003 - Verify Placeholder text "Tracker ID" disappears on entering data in the textbox
     And user clicks Tracker tab
    And user enters tracking ID
    |djkldg#4d9g335|
     Then placeholder text Tracker ID should disappear in textbox
    
@DevTracker   
    Scenario: TC004 - Verify user is able to track order with valid Tracking ID
    And user clicks Tracker tab
    And user enters tracking ID
    |56976af6e2a46a2a|
    And user clicks track order button on tracker page
    Then Tracking ID should be displayed along with status
    
@DevTracker    
    Scenario: TC005 - Verify user is not able to track order with invalid Tracking ID
    And user clicks Tracker tab
    And user enters tracking ID
    |djkldg#4d9g335|
    And user clicks track order button on tracker page
    Then Text should display - no order found
    
@DevTracker    
    Scenario: TC006 - Verify user is not able to search with blank request
     And user clicks Tracker tab
     And user clicks track order button on tracker page
     Then popup appears stating
     |Tracking ID cannot be empty!|
     
@DevTracker     
     Scenario: TC007 - Verify user is able to see three options for order status - Baking, Out for Delivery, You're up Next on Google Map
     And user clicks Tracker tab
     And user enters tracking ID
    |djkldg#4d9g335|
    And user clicks track order button on tracker page
    Then user should be able to see three options for order status - Baking, Out for Delivery, You're up Next on Google Map
    
    
    Scenario: Verify Placeholder text "Tracking ID" should be displayed in the search textbox
    And user clicks Tracker tab
    And user enters tracking ID
    |90419ca72b516dfa|
    And user clicks track order button on tracker page
    Then Tracking ID should be displayed along with status
    
    
    Scenario: Verify user is able to track order on google maps
    And user clicks Tracker tab
    And user enters tracking ID
    |90419ca72b516dfa|
    And user clicks track order button on tracker page
    Then store pointer should be displayed on google maps
    
@DevTracker    
    Scenario: Verify tracker gets updated in accordance with pickup and
 delivery tracking id respectively
 And user clicks Tracker tab
    And user enters tracking ID
    |56976af6e2a46a2a|
   And user clicks track order button on tracker page
   And user removes tracker ID for Delivery 
    And user enters tracking ID
   |90419ca72b516dfa|
    And user clicks track order button on tracker page
    Then Tracking ID should be displayed along with status
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    