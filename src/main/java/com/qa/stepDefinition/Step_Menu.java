package com.qa.stepDefinition;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.asserts.SoftAssert;

import com.qa.pages.Menu;
import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.utils.TestBase;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;



public class Step_Menu extends TestBase{
	
	
	WebDriver driver = getDriver();

	Menu menu=new Menu(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
//	SoftAssert softAssertion= new SoftAssert();
	Logger log= Logger.getLogger("shauryaLogger");

	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	
	@And("^user clicks on Menu tab$")
	public void click_menu() throws Throwable {
		
wait.until(ExpectedConditions.elementToBeClickable(menu.menu_tab));
		
		menu.menu_tab.click();
		//softAssertion.assertAll();
	}
	
	
	@Then("^user should be able to access Menu tab successfully$")
	public void verify_menu_page() throws Throwable{
		wait.until(ExpectedConditions.visibilityOf(menu.menu_page));
		Assert.assertTrue(menu.menu_page.isDisplayed());
		log.info("Menu page displayed successfully");
	}
	
	@Then("all product menu images should be displayed") 
	public void verify_product_images() throws Throwable{
		
		s.validateInvalidImages(driver);
	}

}
