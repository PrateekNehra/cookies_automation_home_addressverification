package com.qa.stepDefinition;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import com.qa.pages.Order;
import com.qa.pages.ShipCookies;
import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.pages.Gear;

public class Step_Gear extends TestBase {
	
	WebDriver driver = getDriver();

	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
	ShipCookies sc = new ShipCookies(driver);
	Gear gear = new Gear(driver);

	// Logger log= Logger.getLogger(step_Order.class);

	Logger log = Logger.getLogger("shauryaLogger");

	WebDriverWait wait = new WebDriverWait(driver, 45);
	Actions act= new Actions(driver);
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	
	@And("^user clicks Gear option$") 
	public void click_gear() throws Throwable{
		
		wait.until(ExpectedConditions.elementToBeClickable(gear.gear_tab));
		gear.gear_tab.click();
		
	}
	
	@When("^user selects moon tee$")
	public void user_selects_moon_tee() throws Throwable {
		
		
	   try{
		   act.moveToElement(gear.ic_moon_tee).build().perform();
		wait.until(ExpectedConditions.elementToBeClickable(gear.ic_moon_tee));
		gear.ic_moon_tee.click();
	   }catch(Exception e){
		   e.printStackTrace();
	   }
		
		
	}

	@When("^user clicks add product button$")
	public void user_clicks_add_product_button() throws Throwable {
		
		//action.moveToElement(gear.add_product_button).build().perform();
		wait.until(ExpectedConditions.elementToBeClickable(gear.add_product_button));
		gear.add_product_button.click();
	}

}
