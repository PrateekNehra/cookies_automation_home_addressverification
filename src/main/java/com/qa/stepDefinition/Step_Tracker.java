package com.qa.stepDefinition;


import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.Order;
import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.pages.Tracker;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class Step_Tracker extends TestBase{
	
	
	WebDriver driver = getDriver();

	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
	Tracker tracker = new Tracker(driver);

	WebDriverWait wait = new WebDriverWait(driver, 60);
	Logger log = Logger.getLogger("shauryaLogger");

	@And("^user clicks Tracker tab$")
	public void click_tracker() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(tracker.tracker_tab));
		tracker.tracker_tab.click();
		log.debug("Tracker is clicked");
	}

	@Then("^Tracker page should be displayed successfully to the user$")
	public void verify_tracker_page() throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.tracker_no_verify));
		Assert.assertTrue(order.tracker_no_verify.isDisplayed());
		log.debug("Trackeing ID textbox is displayed");
	}

	@Then("^placeholder text Tracker ID should be displayed in textbox$")
	public void tracker_placeholder() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.tracker_no_verify));
		String s = order.tracker_no_verify.getAttribute("placeholder");
		System.out.println(s);
		Assert.assertEquals(s, "Tracker ID");

	}

	@And("^user enters tracking ID$")
	public void user_enters_tracking_ID(DataTable arg1) throws Throwable {
		
		wait.until(ExpectedConditions.elementToBeClickable(order.tracker_no_verify));
      List<List<String>> data= arg1.raw();
      order.tracker_no_verify.sendKeys(data.get(0).get(0));
      
	}

	@Then("^placeholder text Tracker ID should disappear in textbox$")
	public void placeholder_text_Tracker_ID_should_disappear_in_textbox() throws Throwable {
     
		Thread.sleep(3000);
		System.out.println("placeholder value : " +order.tracker_no_verify.getAttribute("value"));
		Assert.assertNotEquals(order.tracker_no_verify.getAttribute("placeholder"), order.tracker_no_verify.getAttribute("value"));
		log.debug("placeholder text Tracker ID disappears in textbox");
	}
	
	@And("^user clicks track order button on tracker page$")
	public void user_clicks_track_order_button_on_tracker_page() throws Throwable{
		
		wait.until(ExpectedConditions.elementToBeClickable(tracker.track_order_button));
		tracker.track_order_button.click();
		log.debug("track order button is clicked on tracker page");
	}
	
	@Then("^Text should display - no order found$")
	public void text_displayed() throws Throwable{
		
		wait.until(ExpectedConditions.elementToBeClickable(tracker.tracker_message));
		Assert.assertEquals("No order found!", tracker.tracker_message.getText());
		log.debug("tracker displays message - No order found!");
	}
	
	@Then("^popup appears stating$")
	public void popup_should_appear_stating$(DataTable table) throws Throwable {
		
		List<List<String>> data = table.raw();
		
		wait.until(ExpectedConditions.visibilityOf(signup.popup));
			Assert.assertEquals(data.get(0).get(0), signup.popup.getText());
			System.out.println(signup.popup.getText());
	}
	
	@Then("^user should be able to see three options for order status - Baking, Out for Delivery, You're up Next on Google Map$")
	public void user_should_be_able_to_see_three_options_for_order_status_Baking_Out_for_Delivery_You_re_up_Next_on_Google_Map() throws Throwable {
		
		wait.until(ExpectedConditions.visibilityOf(tracker.baking));
		Assert.assertTrue(tracker.baking.isDisplayed());
		log.debug("Order status- Baking is displayed on Google Maps");
		
		wait.until(ExpectedConditions.visibilityOf(tracker.out_for_delivery));
		Assert.assertTrue(tracker.out_for_delivery.isDisplayed());
		log.debug("Order status- Out for Delivery is displayed on Google Maps");
		
		wait.until(ExpectedConditions.visibilityOf(tracker.you_are_up_next));
		Assert.assertTrue(tracker.you_are_up_next.isDisplayed());
		
		log.debug("Order status- You are up next is displayed on Google Maps");
	}
	
	@Then("^store pointer should be displayed on google maps$")
	public void store_pointer() throws Throwable{
		Thread.sleep(5000);
		Actions action = new Actions(driver);
		 
        action.moveToElement(tracker.store_pointer).build().perform();
        
        Thread.sleep(2000);
        tracker.store_pointer.click();
		
        wait.until(ExpectedConditions.visibilityOf(tracker.store_name));
		Assert.assertTrue(tracker.store_name.isDisplayed());
		log.debug("store name is displayed on Google Maps on clicking store pointer. Store Name : "+tracker.store_name.getText());
		
	}
	@And("^user removes tracker ID for Delivery$")
	public void remove_tracking_id() throws Throwable{
		
		Thread.sleep(3000);
		order.tracker_no_verify.clear();
	}
	

}
