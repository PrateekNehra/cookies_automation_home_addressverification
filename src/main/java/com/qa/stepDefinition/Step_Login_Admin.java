package com.qa.stepDefinition;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.AdminStores;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Login_Admin extends TestBase{
	
	
	WebDriver driver = getDriver();
	AdminStores adminStores = new AdminStores(driver);
	WebDriverWait wait = new WebDriverWait(driver, 30);
	String admin_url = getAdminUrl();
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	Actions action =  new Actions(driver);
	
	@Given("^user opens the ADMIN site$")
	public void user_opens_the_ADMIN_site() throws Throwable {
		try{
		driver.get(admin_url);
		}
		catch (Exception e){
			e.printStackTrace();
			driver.navigate().refresh();
		}
	}
	
	@Given("^verify if user logged in$")
	public void user_Loggedin() throws InterruptedException{
		adminStores.Admin_login();
	}


	@When("^user clicks stores$")
	public void user_click_stores() throws Throwable {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOf(adminStores.stores));
		adminStores.stores.click();
	}
	
  	@And("^user search for store$")
	public void user_search_for_store(DataTable dt) throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(adminStores.storeSearchTextBox));
		List<List<String>> list = dt.raw();
		//String storeName = list.get(0);
		adminStores.storeSearchTextBox.sendKeys(list.get(0).get(0));
  	}

	@And("^user click payments$")
	public void user_click_payments() throws Throwable {
		adminStores.paymentsLink.click();
	}
	
	@Then("^verify card status should be Active$")
	public void verify_CardStatus(DataTable dt){
		List<List<String>> list = dt.raw();
		wait.until(ExpectedConditions.visibilityOf(adminStores.headerStatus));
		String status = adminStores.cardStatus.getText();
		Assert.assertEquals("++++++ Card Status is not Active  +++++",list.get(0).get(0), status);		
	}

	@Then("^verify by default pnp is selected as credit card payment processor$")
	public void default_pnp_isSelected() throws InterruptedException {
		
		try{
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='selected-payment-processor']")));
	    wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
		Select selectedPaymentProcessor = new Select(adminStores.ccPaymentProcessor);
		String str = selectedPaymentProcessor.getFirstSelectedOption().getText();
		Assert.assertEquals("pnp",str);
		}catch(Exception e){
			e.printStackTrace();
		}
//		finally{
//			wait.until(ExpectedConditions.elementToBeClickable(adminStores.profileIcon));
//			adminStores.profileIcon.click();
//			wait.until(ExpectedConditions.elementToBeClickable(adminStores.logout));
//			adminStores.logout.click();
//			Thread.sleep(2000);
////			wait.until(ExpectedConditions.visibilityOf(adminStores.confirmLogout));
//			driver.switchTo().alert();
//			adminStores.confirmLogout.click();
//			
//		}
		}
	
	@And("^user change Credit Card Processor to PNP$")
	public void change_CC_Processor_to_PNP(DataTable dt){
		List<List<String>> list = dt.raw();
		wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
		Select selectedPaymentProcessor = new Select(adminStores.ccPaymentProcessor);
		selectedPaymentProcessor.selectByVisibleText(list.get(0).get(0));
	}
	
	@And("^user change Credit Card Processor to chase$")
	public void change_CC_Processor_to_chase(DataTable dt){
		List<List<String>> list = dt.raw();
		wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
		Select selectedPaymentProcessor = new Select(adminStores.ccPaymentProcessor);
		selectedPaymentProcessor.selectByVisibleText(list.get(0).get(0));
	}
	
	@And("^user clicks Admin Update button$")
	public void clicks_Update_Button(){
		adminStores.updateBtn.click();
	}
	
	@Then("^Verify credit card Processor succesfully changed$")
	public void update_Success() throws InterruptedException{
	Thread.sleep(2000);
	Assert.assertEquals("Payment processor updated", adminStores.popupUpdateSuccessMsg.getText());
	}
	
	@Then("^verify by default chase is selected as CC processor else update to chase$")
	public void verify_ChaseisSelected(DataTable dt) throws InterruptedException{
		    List<List<String>> list = dt.raw();
		    
			wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
			Select selectedPaymentProcessor = new Select(adminStores.ccPaymentProcessor);
			String str = selectedPaymentProcessor.getFirstSelectedOption().getText();
			if(str.equals("chase")){
				System.out.println("chase is already selected");
			
		}else{
			wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
			//Select selectedPaymentProcessor1 = new Select(adminStores.ccPaymentProcessor);
			Thread.sleep(3000);
			selectedPaymentProcessor.selectByVisibleText(list.get(0).get(0));
//			action.moveToElement(adminStores.updateBtn).build().perform();
//			jse.executeScript("arguments[0].click();", adminStores.updateBtn);
			adminStores.updateBtn.click();
			Thread.sleep(2000);
			Assert.assertEquals("Payment processor updated", adminStores.popupUpdateSuccessMsg.getText());
		}

		
	}
	
	@Then("^verify by default pnp is selected as CC processor else update to pnp$")
	public void verify_PNPisSelected(DataTable dt) throws InterruptedException{
	    List<List<String>> list = dt.raw();
	    
		wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
		Select selectedPaymentProcessor = new Select(adminStores.ccPaymentProcessor);
		String str = selectedPaymentProcessor.getFirstSelectedOption().getText();
		if(str.equals("pnp")){
			System.out.println("pnp is already selected");
		
	}else{
		wait.until(ExpectedConditions.visibilityOf(adminStores.ccPaymentProcessor));
		//Select selectedPaymentProcessor1 = new Select(adminStores.ccPaymentProcessor);
		Thread.sleep(3000);
		selectedPaymentProcessor.selectByVisibleText(list.get(0).get(0));
		
		adminStores.updateBtn.click();
		Thread.sleep(2000);
		Assert.assertEquals("Payment processor updated", adminStores.popupUpdateSuccessMsg.getText());
	}
	
}

}
