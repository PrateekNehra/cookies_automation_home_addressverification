package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class Gear extends TestBase{
	
	
	@FindBy(xpath = "//a[contains(text(),'Gear')]")
	public WebElement gear_tab;
	
	@FindBy(xpath = "//div[@id='adult']//div[2]//div[1]//div[1]//img[1]")
	public WebElement ic_moon_tee;
	
	@FindBy(xpath = "//div[contains(@class,'mt-4 d-none d-lg-block')]//button[contains(@class,'btn btn-primary btn-block options-submit-btn')][contains(text(),'Add Product')]")
	public WebElement add_product_button;
	
	
	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);

	public Gear(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);

	}

}
