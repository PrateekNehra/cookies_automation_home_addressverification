@Address2
Feature: Order

  Background: user access to website
    
  
  #| url | https://5.stage.insomniacookies.com/ |

  Scenario Outline: Enter address and verify Delivery option is present and if present take screenshot
  	Given User opens the Website1
    Given user clicks on Order button
    When user enters the Address as "<address>"
    Then Verify if Delivery button is present and get store name if delivery button is present
    
    Examples:
| address |
|Eastway Drive,Kent,OH,44240|
|27 North Court Street,Athens,OH,45701|
|609 South Lincoln Street,Kent,OH,44240|
|244 Sherman Avenue,New Haven,CT,06511|
|3320 Powelton Avenue,Philadelphia,PA,19104|
|3990 Fifth Avenue,Pittsburgh,PA,15213|
|17601 Cannon Avenue,Lakewood,OH,44107|
|1900 South Floyd Street,Louisville,KY,40208|
|628 Joyner Street,Greensboro,NC,27403|
|25 Catherine Street,West Haven,CT,06516|
|1600 East Washington Street,Greensboro,NC,27401|
|2223 Greenville Boulevard Northeast,Greenville,NC,27858|
|55 John Street,New York,NY,10038|
|42 Grosvenor Street,Athens,OH,45701|
|East 5th Street,Greenville,NC,27858|
|2 Dudley Street,Providence,RI,02905|
|234 Wilson Road,East Lansing,MI,48825|
|L. Richard Belden Hall,Mansfield,CT,06269|
|2566 West Tennessee Street,Tallahassee,FL,32304|
|1260  Holmes Road,Ypsilanti,MI,48198|
|906 Howard Street,Greensboro,NC,27403|
|421 Harding Avenue,Morgantown,WV,26505|
|1450 Chapel Street,New Haven,CT,06511|
|1706 West Diamond street,Philadelphia,PA,19121|
|1515 North McKinley Avenue,Muncie,IN,47306|
|3621  Spring Garden Street,Philadelphia,PA,19104|
|331 Catherine Street,Ann Arbor,MI,48104|
|56 West Gay Street,Harrisonburg,VA,22802|
|2419 Circle Street,Wilmington,NC,28403|
|4530- A Lilac Lane,Kalamazoo,MI,49006|
|316 South Webster Avenue,Norman,OK,73069|
|111 East Spring Street,Oxford,OH,45056|
|400 Bonham Road,Columbia,SC,29205|
|1520 Blue Ridge Road,Raleigh,NC,27607|
|120 East 12th Street,New York,NY,10003|
|1505 West Tharpe Street,Tallahassee,FL,32303|
|45 Ivan Allen Junior Boulevard Northwest,Atlanta,GA,30308|
|100  Bleecker Street,New York,NY,10012|
|55 Brittany Residence Hall East 10th Street,New York,NY,10003|
|10 Merrill Science Drive,Amherst,MA,01002|
|128 Hawley Street,Binghamton,NY,13901|
|21 North Juniper Street,Philadelphia,PA,19107|
|1811 Walker Avenue,Greensboro,NC,27403|
|340 Hilltop Avenue,Lexington,KY,40508|
|1415 8th Street,Tuscaloosa,AL,35401|
|1437 West 40th Street,Norfolk,VA,23508|
|5306 Heatherstone Drive,Baton Rouge,LA,70820|
|515 North Main Street,Bloomington,IL,61701|
|756 N Mansfield St,Ypsilanti,MI,48197|
|10010 Woodberry Trail Lane,Charlotte,NC,28262|
|900 East Park Street,Carbondale,IL,62901|
|2014 Hawthorne Court,Ames,IA,50010|
|12W East Union Street,Athens,OH,45701|
|1428 Northwest 27th Street,Oklahoma City,OK,73106|
|838  Overholt Road,Kent,OH,44240|
|805 Milton Street,Greensboro,NC,27403|
|201 West Green Street,Champaign,IL,61820|
|1919 34th Street,Lubbock,TX,79411|
|901 Mosley Street,Tallahassee,FL,32310|
|2421 Woodway Drive,Manhattan,KS,66502|
|1334 West Friendly Avenue,Greensboro,NC,27403|
|238 West 20th Street,New York,NY,10011|
|1300  Rollins Street,Columbia,MO,65201|
|East Tower,Ithaca,NY,14850|
|1340 King Avenue,Columbus,OH,43212|
|340 South Brody Road,East Lansing,MI,48825|
|1664 Loblolly Court,Kent,OH,44240|
|70 Morningside Drive,New York,NY,10027|
|401 Boyd Street,Norman,OK,73069|
|3825 University Drive C,Pittsburgh,PA,15261|
|4625 Burbank Drive,Baton Rouge,LA,70820|
|1106 Gold Rush Boulevard,Charlotte,NC,28262|
|611  East University Avenue,Ann Arbor,MI,48104|
|1100  Chancellor Park Drive,Charlotte,NC,28213|
|263 Calhoun Street,Cincinnati,OH,45219|
|2108 Unity Place,Louisville,KY,40208|
|7001 North Charles Street,Towson,MD,21204|
|1345 Benning Place Northeast,Atlanta,GA,30307|
|2915 Cedar Creek Road,Greenville,NC,27834|
|1911 7th Street,Lubbock,TX,79401|
|860 Briarcliff Road Northeast,Atlanta,GA,30306|
|15 Morgan Avenue,Starkville,MS,39759|
|1351 Mason Farm Road,Chapel Hill,NC,27514|
|1301 27th Place South,Birmingham,AL,35205|
|1811 Lambeth Lane,Charlottesville,VA,22903|
|417 North Comanche Street,San Marcos,TX,78666|
|438  Oakland Street,Morgantown,WV,26505|
|616 East Green Street,Champaign,IL,61820|
|Risley Hall, ,Ithaca,NY,14850|
|1465 South Grand Boulevard,St. Louis,MO,63104|
|4124 West 4th Street,Hattiesburg,MS,39401|
|600 Gresham Drive,Norfolk,VA,23507|
|606 Park Lane,Oxford,MS,38655|
|1802 5th Avenue East,Tuscaloosa,AL,35401|
|910 South 3rd Street,Champaign,IL,61820|
|730 Crestwood Drive,Manhattan,KS,66502|
|2601 West Oak Street,Denton,TX,76201|
|1980 Stotts Road,Ames,IA,50010|
|Linden Rd,State College,PA ,16801|
|246 Pheasant Run Drive,Blacksburg,VA,24060|
|925 Bellaire Avenue,State College,PA,16801|
|1019 East Cherry Street,Springfield,MO,65807|
|3535 Nicholson Drive,Baton Rouge,LA,70802|
|1135 High Avenue,Oshkosh,WI,54901|
|3022 Signature Boulevard,Ann Arbor,MI,48103|
|2010 9th Street,Lubbock,TX,79401|
|1002 Lake Street,Kent,OH,44240|
|2025 Gervais Street,Columbia,SC,29204|
|461 Whalley Avenue,New Haven,CT,06511|
|120 Fifth Avenue,Pittsburgh,PA,15222|
|1513 Springbrook Drive,Cedar Falls,IA,50613|
|1163  West Peachtree Street Northeast,Atlanta,GA,30309|
|200 Catherine Street,Starkville,MS,39759|
|221 Langdon Street,Madison,WI,53703|
|1708 Cecil B. Moore Avenue,Philadelphia,PA,19121|
|Sigma Alpha Mu Fraternity Fraternity Road,Rochester,NY,14627|
|731 Bluemont Avenue,Manhattan,KS,66502|
|John Buckley Residence Hall,Mansfield,CT,06269|
|609 South Lincoln Street,Kent,OH,44240|
|348 Blue Course Drive,State College,PA,16803|
|107 North Oak Street,Greenville,NC,27858|
|101  Ojibway Court,Mount Pleasant,MI,48858|
|400 Washington Avenue,St. Louis,MO,63102|
|2305 Everett Avenue,Raleigh,NC,27607|
|1728 Forest Avenue,Knoxville,TN,37916|
|1725 Lafayette Avenue,Kalamazoo,MI,49006|
|361 Bailey Street,East Lansing,MI,48823|
|Lantana Hall, 501 North Edward Gary Street,San Marcos,TX,78666|
|1811 Mercedes Road,Denton,TX,76205|
|2105 Penthouse Drive,Birmingham,AL,35205|
|2721 Hannah Boulevard,East Lansing,MI,48823|
|2115 Bentley Drive,Pittsburgh,PA,15219|
|1777 Chandler Road,Statesboro,GA,30458|
|602 South College Avenue,Columbia,MO,65201|
|1601 Devon Lane,Harrisonburg,VA,22801|
|1607  9th Street,Tuscaloosa,AL,35401|
|1615 Sycamore Avenue,Huntsville,TX,77340|
|1200 West Marshall Street,Richmond,VA,23220|
|902 Drake Street,Madison,WI,53715|
|12 The Green,Newark,DE,19716|
|518 South 3rd Street,Ames,IA,50010|
|385 Misty Lee Lane,Starkville,MS,39759|
|535 West Johnson Street,Madison,WI,53703|
|6045  Delmar Boulevard,St. Louis,MO,63112|
|100  West 3rd Street,Greenville,NC,27858|
|200 Lothrop Street,Pittsburgh,PA,15213|
|627 Cabell Avenue,Charlottesville,VA,22903|
|201 Tallawanda Road,Oxford,OH,45056|
|501 Pritchard Avenue,Chapel Hill,NC,27516|
|210 South Locust Street,Denton,TX,76201|
|612 Walton Avenue,St. Louis,MO,63108|
|2106 Main Street,Lubbock,TX,79401|
|2204  Jackson Avenue West,Oxford,MS,38655|
|34 Wendell Street,Cambridge,MA,02138|
|736  West Main Street,Kent,OH,44240|
|3701 Water Oak Lane,Greenville,NC,27858|
|909  East Shaw Lane,East Lansing,MI,48825|
|51 East Green Street,Champaign,IL,61820|
|107 East 14th Avenue,Columbus,OH,43201|
|76 Conklin Avenue,Binghamton,NY,13903|
|5220 Troost Avenue,Kansas City,MO,64110|
|2323 Glenna Goodacre Boulevard,Lubbock,TX,79401|
|6701 North Charles Street,Towson,MD,21204|
|Unnamed Road,Baton Rouge,LA,70820|
|100 Llano Circle,San Marcos,TX,78666|
|505 1/2 Bowles Street,Normal,IL,61761|
|615 18th Street South,Birmingham,AL,35233|
|401 Van Buren Street,Syracuse,NY,13210|
|1227  Linden Place Northeast,Washington,DC,20002|
|1401 College Avenue,Manhattan,KS,66502|
|1019 Douglas Street,Mount Pleasant,MI,48858|
|1614 12th Street South,Birmingham,AL,35205|
|2202 Mac Davis Lane,Lubbock,TX,79401|
|1600 Sam Houston Avenue,Huntsville,TX,77340|
|2152 Lincoln Way,Ames,IA,50014|
|Eddy Hall,South Kingstown,RI,02881|
|550 South Jackson Street,Louisville,KY,40202|
|777 Ben Hur Road,Baton Rouge,LA,70820|
|2001 13th Avenue South,Birmingham,AL,35205|
|101 Pine Street,Providence,RI,02903|
|445 Paul Hardin Drive,Chapel Hill,NC,27514|
|1050 Piedmont Avenue Northeast,Atlanta,GA,30309|
|100 College Street,New Haven,CT,06510|
|2434 South 6th Street,Terre Haute,IN,47802|
|1818 Chandler Road,Statesboro,GA,30458|
|135 South 18th Street,Philadelphia,PA,19103|
|Bulger Residence Hall, ,Akron,OH,44304|
|683 Riddle Road,Cincinnati,OH,45220|
|404  Emerson Drive,Towson,MD,21252|
|1825 South Crawford Road,Mount Pleasant,MI,48858|
|330 East 80th Street,New York,NY,10075|
|1504 West Riverside Avenue,Muncie,IN,47303|
|2511 Abbot Road,East Lansing,MI,48823|
|1535 2nd Avenue,New York,NY,10075|
|2401 Gillham Road,Kansas City,MO,64108|
|815 North Cedarbrook Avenue,Springfield,MO,65802|
|2015 2015  West University Street West University Street,Springfield,MO,65807|
|2119 Melrose Drive,Champaign,IL,61820|
|200 North 6th Street,Terre Haute,IN,47809|
|102 East Main Street,Newark,DE,19711|
|1031 13th Street East,Tuscaloosa,AL,35404|
|3650 Nicholson Drive,Baton Rouge,LA,70802|
|210 15th Street East,Tuscaloosa,AL,35401|
|207 Greene Street,Chapel Hill,NC,27516|
|Hamilton Hall, ,State College,PA,16801|
|1700  6th Avenue South,Birmingham,AL,35233|
|707 East Merry Avenue,Bowling Green,OH,43403|
|3935 Jennings Drive,Cedar Falls,IA,50613|
|3006 Clearwater Avenue,Bloomington,IL,61704|
|218 State Street,Lexington,KY,40503|
|846 South Robberson Avenue,Springfield,MO,65806|
|22  South Carroll Street,Madison,WI,53703|
|4075 South Isabella Road,Mount Pleasant,MI,48858|
|810 Saint Vincents Drive,Birmingham,AL,35205|
|2434  South 6th Street,Terre Haute,IN,47802|
|Kirby-Smith Hall,Baton Rouge,LA,70802|
|238 University Avenue,Towson,MD,21204|
|3200 North Alafaya Trail,Orlando,FL,32826|
|East Circle Drive,Ypsilanti,MI,48197|
|Unnamed Road,Bryn Mawr,PA,19010|
|616 South Ramsey Street,Stillwater,OK,74074|
|404  Emerson Drive,Towson,MD,21252|
|2103 Jasmine Street,Denton,TX,76205|
|101 Davis Street,Athens,GA,30606|
|331 Lawn Street,Pittsburgh,PA,15213|
|3676 Libra Drive,Orlando,FL,32826|
|420 East 54th Street,New York,NY,10022|
|1021 East Harrison Street,Springfield,MO,65807|
|582 East McKinley Street,Baton Rouge,LA,70802|
| East Drive,Vestal,NY,13850|
|1300 West 23rd Street,Cedar Falls,IA,50614|
|600 East Madison Street,Ann Arbor,MI,48109|
| Unnamed Road,Lawrence Township,NJ,08648|
|745 Colonial Drive,Tuscaloosa,AL,35401|
|1737 Northwest 17th Street,Oklahoma City,OK,73106|
|340 South Brody Road,East Lansing,MI,48825|
|317 South Avenue,Springfield,MO,65806|
|6186 Waterman Boulevard,St. Louis,MO,63112|
|100 Faculty Row,Oxford,MS,38655|
|417 South Council Street,Muncie,IN,47305|
|441 Bluestone Drive,Harrisonburg,VA,22807|
|1163  West Peachtree Street Northeast,Atlanta,GA,30309|
|Kirby-Smith Hall,Baton Rouge,LA,70802|
|200 Water Street,New York,NY,10038|
|Kirkland House, 95 Dunster Street,Cambridge,MA,02138|
|505 Epsom Road,Towson,MD,21286|
|130  Homestead Road,Mankato,MN,56001|
|21 8th Street North,Fargo,ND,58102|
|1709 East Lindsey Street,Norman,OK,73071|
|239 Ruth Street,Athens,GA,30601|
      
    