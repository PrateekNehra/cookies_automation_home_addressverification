@Address5
Feature: Order

  Background: user access to website
    
  
  #| url | https://5.stage.insomniacookies.com/ |

  Scenario Outline: Enter address and verify Delivery option is present and if present take screenshot
  	Given User opens the Website1
    Given user clicks on Order button
    When user enters the Address as "<address>"
    Then Verify if Delivery button is present and get store name if delivery button is present
    
    Examples:
| address |
|18614 Detroit Avenue,Lakewood,OH,44107|
|11 Broadway,New York,NY,10004|
|1200 East Marshall Street,Richmond,VA,23298|
|825 East Robinson Street,Norman,OK,73071|
|3440 Market Street,Philadelphia,PA,19104|
|11 Wall Street,New York,NY,10005|
|2 Park Avenue,New York,NY,10016|
|411 East Market Street,Akron,OH,44304|
|153 East Exchange Street,Akron,OH,44302|
|1116 East 59th Street,Chicago,IL,60637|
|518 E Street,Davis,CA,95616|
|309  Lynn Avenue,Ames,IA,50014|
|211 East 43rd Street,New York,NY,10017|
|68  High Street,New Haven,CT,06511|
|731 Lexington Avenue, ,New York,NY,10022|
| Stout Lane,Stillwater,OK,74075|
|100 West 84th Street,New York,NY,10024|
|500 Inman Street,Denton,TX,76205|
|710 West Franklin Street,Richmond,VA,23220|
|611 5th Avenue,New York,NY,10022|
|160 Varick Street,New York,NY,10013|
|Providence County Courthouse, 250 Benefit Street,Providence,RI,02903|
|3166 Mount Pleasant Street Northwest,Washington,DC,20010|
|2020  York Road,Raleigh,NC,27608|
|101 Helen Keller Boulevard,Tuscaloosa,AL,35404|
|2067 South Ogden Street,Denver,CO,80210|
|412 Broadway,New York,NY,10013|
|720  Woodland Avenue,Lexington,KY,40508|
|4510 Belleview Avenue,Kansas City,MO,64111|
|901 Paul William Bryant Drive,Tuscaloosa,AL,35401|
|1348 Bainbridge Street,Philadelphia,PA,19147|
|2018 Clinch Avenue,Knoxville,TN,37916|
|1910 Caledonia Avenue,Knoxville,TN,37916|
|3732 Locust Walk,Philadelphia,PA,19104|
|1800 Kimball Avenue,Manhattan,KS,66502|
|2 Gold Street,New York,NY,10038|
|700 East Saint Louis Street,Springfield,MO,|
|1737 McGee Street,Kansas City,MO,64108|
|4600 Madison Avenue,Kansas City,MO,64112|
|2561  Lac De Ville Boulevard,Rochester,NY,14618|
|1302 Texas Avenue,Lubbock,TX,79401|
|346 East Lee Street,Louisville,KY,40208|
|401 East Franklin Avenue,El Paso,TX,79901|
|77 East Mill Street,Akron,OH,44308|
|507 South Locust Street,Champaign,IL,61820|
|304 Felmley Drive,Normal,IL,61761|
|South Trust Tower, 1201 Main Street,Columbia,SC,29201|
|8204  Baltimore Avenue,College Park,MD,20740|
|200 Lothrop Street,Pittsburgh,PA,15213|
|953 Danby Road,Ithaca,NY,14850|
|396 Oconee Street,Athens,GA,30601|
|Campus Drive,Allendale Charter Township,MI,49401|
|169  Riverside Drive,Binghamton,NY,13905|
|800 West Saint Clair Avenue,Cleveland,OH,44113|
|212 South Elm Street,Denton,TX,76201|
|1131  Jackson Avenue,Tuscaloosa,AL,35401|
|401 5th Avenue,New York,NY,10016|
|601 West 26th Street,New York,NY,10001|
|110 Memorial Hospital Drive,Huntsville,TX,77340|
|3305 31st Street,Lubbock,TX,79410|
|121 South Fremont Avenue,Baltimore,MD,21201|
|261 West 112th Street,New York,NY,10026|
|701 Fulton Street Southeast,Minneapolis,MN,55455|
|250 Park Avenue,New York,NY,10177|
|1704  State Street,Cedar Falls,IA,50613|
|500 Koehler Drive,Morgantown,WV,26508|
|420 East 55th Street,New York,NY,10022|
|Conner Hall, ,Oxford,MS,38677|
|325  East 80th Street,New York,NY,10075|
|214 Russell Street,Starkville,MS,39759|
|700 Northeast 13th Street,Oklahoma City,OK,73104|
|28 West 23rd Street,New York,NY,10010|
|4921 Parkview Place,St. Louis,MO,63110|
|3839 Hunsaker Street,East Lansing,MI,48823|
|101 Manning Drive,Chapel Hill,NC,27514|
|201 Gray Avenue,Ames,IA,50014|
|602 Indiana Avenue,Lubbock,TX,79415|
|2148  Jackson Avenue West,Oxford,MS,38655|
|1437 Post Road,San Marcos,TX,78666|
|300 Madison Avenue,New York,NY,10017|
|1018 East Davie Street,Raleigh,NC,27601|
|1104 West Nevada Street,Urbana,IL,61801|
|615 Downingtown Pike,West Chester,PA,19380|
|1636 South Glenstone Avenue,Springfield,MO,65804|
|319 West Oak Street,Denton,TX,76201|
|Shively Sports Center, 685 Sports Center Drive,Lexington,KY,40502|
|46  Prince Street,New Haven,CT,06519|
|6333 North Winthrop Avenue,Chicago,IL,60660|
|1560  Watson Road,Mount Pleasant,MI,48858|
|1117  North Dearborn Street,Chicago,IL,60610|
|479 Van Voorhis Road,Morgantown,WV,26505|
|1128 West Marsh Street,Muncie,IN,47303|
|2301 Richard Arrington Junior Boulevard North,Birmingham,AL,35203|
|17  Ross-Ade Drive,West Lafayette,IN,47906|
|13 Crosby Street,New York,NY,10013|
|311 West Walton Street,Chicago,IL,60610|
|2424  East 55th Street,Indianapolis,IN,46220|
|Evans Hall, 165 Whitney Avenue,New Haven,CT,06511|
|1567 Devon Lane,Harrisonburg,VA,22801|
|532 Moye Boulevard,Greenville,NC,27834|
|21 Jefferson Place,Athens,GA,30601|
|151 North Stone Avenue,Tucson,AZ,85701|
|412  West 25th Street,New York,NY,10001|
|200 West Street, ,New York,NY,10282|
|730 Madison Avenue,Charlottesville,VA,22903|
|315 North 12th Street,Philadelphia,PA,19107|
|215 West 104th Street,New York,NY,10025|
|25 West Rosedale Avenue,West Chester,PA,19382|
|599 Lexington Avenue, 599 Lexington Avenue,New York,NY,10022|
|20  York Street,New Haven,CT,06510|
|2195 West Tennessee Street,Tallahassee,FL ,32304|
|707 Ridge Street,Bowling Green,OH,43403|
|1737 Main Street,Kansas City,MO,64108|
|615 West Johnson Street,Madison,WI,53706|
|1100 West Avenue,Miami Beach,FL,33139|
|1312 Locust Street,Kansas City,MO,64106|
|301 West Main Street,Richmond,VA,23284|
|401 Brewster Van Buren Street,Syracuse,NY,13210|
|1040 East Elm Street,Springfield,MO,65806|
|429 Forbes Avenue,Pittsburgh,PA,15219|
|520 West 112th Street,New York,NY,10025|
|1405 University Walk Circle,Charlotte,NC,28213|
|520 Madison Avenue,New York,NY,10022|
|1813 West 8th Street,Cedar Falls,IA,|
|475 Schleman Hall of Student Services West Stadium Avenue,West Lafayette,IN,47907|
|4400  Vestal Parkway East,Vestal,NY,13850|
|720 Northwestern Avenue,West Lafayette,IN,47906|
|3 Park Avenue,New York,NY,10016|
|32 Old Slip,New York,NY,10005|
|185 Exchange Boulevard,Rochester,NY,14614|
|2000 Pennington Road,Ewing Township,NJ,08618|
| 3400 Civic Center Boulevard,Philadelphia,PA,19104|
|320 East State Street,West Lafayette,IN,47906|
|901  South Bond Street,Baltimore,MD,21231|
|1215  Lee Street,Charlottesville,VA,22908|
|601 University Drive,San Marcos,TX,78666|
|2200 South Sangre Road,Stillwater,OK,74074|
|919 Third Avenue, ,New York,NY,10022|
|219 North Broad Street,Philadelphia,PA,19107|
|1910 Blanding Street,Columbia,SC,29201|
|Flanner Hall, 1032 West Sheridan Road,Chicago,IL,60660|
|723  South Lewis Street,Stillwater,OK,74074|
|60  Thompson Street,New York,NY,10012|
|1201 Elizabeth Avenue,Charlotte,NC,28204|
|1523 East Eastern Avenue,Stillwater,OK,74074|
|1020 Main Campus Drive,Raleigh,NC,27606|
|385 Tallawanda Road,Oxford,OH,45056|
|715 College of Design Bissell Road,Ames,IA,50011|
|1 Saint Joseph Drive,Lexington,KY,40504|
|3459 Fifth Avenue,Pittsburgh,PA,15213|
|31 Welsford Street,Pittsburgh,PA,15213|
|4400 Vestal Parkway East,Vestal,NY,13850|
|6439 Garners Ferry Road,Columbia,SC,29209|
|158 Henry Street,Binghamton,NY,13901|
|319 Pratt Drive,Indiana,PA,15701|
|2136 West College Street,Springfield,MO,65806|
|10 Dorrance Street,Providence,RI,02903|
|3333 Cincinnati Children's Hospital Medical Center Location B Burnet Avenue,Cincinnati,OH,45229|
|100 East Penn Square,Philadelphia,PA,19107|
|225 South 18th Street,Philadelphia,PA,19103|
|528 Northwest 18th Street,Oklahoma City,OK,73103|
|535 Madison Avenue,New York,NY,10022|
|1550 Eddie Robinson Sr Drive,Baton Rouge,LA,70802|
|114 East 25th Street,New York,NY,10010|
|2420 Lincoln Way,Ames,IA,50014|
|4401 Wornall Road,Kansas City,MO,64111|
|2970 Market Street,Philadelphia,PA,19104|
|620  Greensboro Avenue,Tuscaloosa,AL,35401|
|2250 Patterson Street,Eugene,OR,97405|
|Fernow Street,Clemson,SC,29631|
|616 Northwest 18th Street,Oklahoma City,OK,73103|
|437 North Frances Street,Madison,WI,53703|
|2901 5th Avenue,Fort Worth,TX,76110|
|College of Business Administration Building, 259 South Broadway Street,Akron,OH,44308|
|2210 Main Street,Lubbock,TX,79401|
|140 M Street Northeast,Washington,DC,20002|
|917 13th Street South,Birmingham,AL,35294|
|810 East Byrd Street,Richmond,VA,23219|
|1001 Ocala Road,Tallahassee,FL,32304|
|1 Saint Joseph Drive,Lexington,KY,40504|
|30 Union Station West Pershing Road,Kansas City,MO,64108|
|635 East Hamilton Avenue,State College,PA,16801|
|4116 Spruce Street,Philadelphia,PA,19104|
|801 Hillsborough Street,Raleigh,NC,27603|
|10295 48th Avenue,Allendale Charter Township,MI,49401|
|119 Dapple Court,Wilmington,NC,28403|
|6511 North Sheridan Road,Chicago,IL,60626|
|85 Adams Street,Rochester,NY,14608|
|608 Fulton Street,Greensboro,NC,|
|117 South 13th Street,Philadelphia,PA,19107|
|1111 Amsterdam Avenue,New York,NY,10025|
|101 Manning Drive,Chapel Hill,NC,|
|300  Washington Avenue Southeast,Minneapolis,MN,55455|
|4015 22nd Place,Lubbock,TX,79410|
|1500 Abbot Road,East Lansing,MI,48823|
|225 Broadway,New York,NY,10007|
|4000 Central Florida Blvd,Orlando,FL,32816|
|200 Varick Street,New York,NY,10014|
|256 Crittenden Boulevard,Rochester,NY,14642|
|2150 Anderson Road,Oxford,MS,38655|
|1819 5th Avenue North,Birmingham,AL,35203|
|321 North Clark Street,Chicago,IL,60654|
|400 East 11th Street,Kansas City,MO,64105|
|1403 Circle Drive,Knoxville,TN,37916|
|11 Fraternity Row,College Park,MD,20740|
|16 North Hadley Road,Amherst,MA,01002|
|5637 Carrollton Avenue,Indianapolis,IN,46220|
|423 West Broadway,New York,NY,10012|
|768 Yale Street,Akron,OH,44311|
|351 Foreman Avenue,Lexington,KY,40508|
|1621 Crumley Hall West Highland Street,Denton,TX,76201|
|802 South Jefferson Street,Stillwater,OK,74074|
|638 Broadway Avenue,Orlando,FL,32803|
|228 Robert S Kerr Avenue,Oklahoma City,OK,73102|
|48 Westminster Drive,Morgantown,WV,26501|
|2110 Avent Ferry Road,Raleigh,NC,27606|
|2101 East Arlington Boulevard,Greenville,NC,27858|
|Rowland Hall,Ithaca,NY,14850|
|465 South Gay Street,Knoxville,TN,37902|
|2000 9th Avenue South,Birmingham,AL,35205|
|1710 1st Avenue North,Birmingham,AL,35203|
|33  East Main Street,Madison,WI,53703|
|,undefined,und,undefined|
|,undefined,und,undefined|
|1018 East Davie Street,Raleigh,NC,27601|
|One Kansas City Place, 1200 Main Street,Kansas City,MO,64105|
|170 East Quad,Davis,CA,95616|
|363 Richland Avenue,Athens,OH,45701|
|1312 Colbury Road,Towson,MD,21239|
|610  Purdue Mall,West Lafayette,IN,47907|
|1636 North Wells Street,Chicago,IL,60614|
|12039 Red Ibis Lane,Orlando,FL,32817|
|151 Engineer's Way,Charlottesville,VA,22911|
|17358 Georgia 67,Statesboro,GA,30458|
|201 East Chestnut Street,Oxford,OH,45056|
|1600 Sam Houston Avenue,Huntsville,TX,77340|
|3405 Main Avenue,Fargo,ND,58103|
|6 South College Street,Athens,OH,45701|
|1 1 New York Plaza FDR Drive,New York,NY,10004|
|521 College Avenue,Clemson,SC,29631|
|1 College Hall,Philadelphia,PA,19104|
|620 Massachusetts Avenue,Amherst,MA,01003|
|800 Hospital Drive,Columbia,MO,65201|
|5825 Indianola Avenue,Indianapolis,IN,46220|
|3200 Burnet Avenue,Cincinnati,OH,45229|
|5042 South Champlain Avenue,Chicago,IL,60615|
|3022 Broadway,New York,NY,10027|
|303 East 57th Street,New York,NY,10022|
|Wilson, 219 Wilson Road,East Lansing,MI,48825|
|80 Church Street,New Haven,CT,06510|
      
    