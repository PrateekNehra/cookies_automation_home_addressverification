@adminTest
Feature: Admin Store:  I want to place order with admin site using credit card for different stores and different CC Processors

  
  Background: user access to ADMIN site
  Given user opens the ADMIN site
 

  Scenario: TC 001 Verify by default payment processor is PNP for store 13th Street
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |13th|
  And user click payments
  Then verify by default pnp is selected as credit card payment processor
  

  Scenario: TC 002 Verify by default payment processor is PNP for store Blacksburg
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Blacksburg|
  And user click payments
  Then verify by default pnp is selected as credit card payment processor
  

  Scenario: TC 003 Verify user is able to select Chase for Bryn Mawr store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Bryn Mawr|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Bryn Mawr|
  And user click payments
  Then verify card status should be Active
  |Active|
  And user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
    
  Scenario: TC 004 Verify user is able to select and update Chase for Midtown East NYC store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Midtown East NYC|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Midtown East NYC|
  And user click payments
  Then verify card status should be Active
  |Active|
  And user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed


  Scenario: TC 005 Verify user is able to select and update Chase for University City North store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |University City North|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |University City North|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 006 Verify user is able to select and update Chase for Amherst store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Amherst|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Amherst|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 007 Verify user is able to select and update Chase for Charles Village store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Charles Village|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Charles Village|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 008 Verify user is able to select and update Chase for Chico, CA store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Chico, CA|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Chico, CA|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 009 Verify user is able to select and update Chase for Fells Point store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Fells Point|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Fells Point|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 010 Verify user is able to select and update Chase for Gear Store store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Gear Store|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Gear Store|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  
  Scenario: TC 011 Verify user is able to select and update Chase for Gifts store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Gifts|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Gifts|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 012 Verify user is able to select and update Chase for Hyde Park store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Hyde Park|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Hyde Park|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 013 Verify user is able to select and update Chase for Knoxville store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Knoxville|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Knoxville|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 014 Verify user is able to select and update Chase for Manayunk store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Manayunk|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Manayunk|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 015 Verify user is able to select and update Chase for New Orleans store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |New Orleans|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |New Orleans|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  
  Scenario: TC 016 Verify user is able to select and update Chase for Poncey Highlands Atlanta store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Poncey Highlands Atlanta|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Poncey Highlands Atlanta|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 017 Verify user is able to select and update Chase for Raleigh store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Raleigh|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Raleigh|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 018 Verify user is able to select and update Chase for Starkville store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Starkville|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Starkville|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 019 Verify user is able to select and update Chase for State College store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |State College|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |State College|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
  Scenario: TC 020 Verify user is able to select and update Chase for Wicker Park Chicago store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Wicker Park Chicago|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Wicker Park Chicago|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
  
   
  Scenario: TC 021 Verify user is able to select and update Chase for Ypsilanti store
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Ypsilanti|
  And user click payments
  Then verify by default pnp is selected as CC processor else update to pnp
  |pnp|
  Then verify if user logged in
  When user clicks stores
  And user search for store
  |Ypsilanti|
  And user click payments
  Then verify card status should be Active
  |Active|
  When user change Credit Card Processor to chase
  |chase|
  And user clicks Admin Update button
  Then Verify credit card Processor succesfully changed
 

  Scenario: TC 022 CC Order placed then Card Processor changed to PNP and a New CC order placed for Store State College
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |State College|
  And user click payments
  Then verify by default chase is selected as CC processor else update to chase
  |chase| 
    Given User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | 421 East Beaver Avenue, State College, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    When user opens the ADMIN site
    And verify if user logged in
    And user clicks stores
    And user search for store
    |State College|
    And user click payments
    And user change Credit Card Processor to PNP
    |pnp|
    And user clicks Admin Update button
    Then Verify credit card Processor succesfully changed
    Then User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | 421 East Beaver Avenue, State College, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    

  Scenario: TC 023 CC Order placed then Card Processor changed to PNP and a New CC order placed for Store Starkville
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Starkville|
  And user click payments
  Then verify by default chase is selected as CC processor else update to chase
  |chase| 
    Given User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | 87 Mill Street, Starkville, MS 39759 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    When user opens the ADMIN site
    And verify if user logged in
    And user clicks stores
    And user search for store
    |Starkville|
    And user click payments
    And user change Credit Card Processor to PNP
    |pnp|
    And user clicks Admin Update button
    Then Verify credit card Processor succesfully changed
    Then User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | 87 Mill Street, Starkville, MS 39759 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
 
  Scenario: TC 024 CC Order placed then Card Processor changed to PNP and a New CC order placed for Store Ypsilanti
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Ypsilanti|
  And user click payments
  Then verify by default chase is selected as CC processor else update to chase
  |chase| 
    Given User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | Ypsilanti Vision, Washtenaw Avenue, Ypsilanti, MI 48197 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    When user opens the ADMIN site
    And verify if user logged in
    And user clicks stores
    And user search for store
    |Ypsilanti|
    And user click payments
    And user change Credit Card Processor to PNP
    |pnp|
    And user clicks Admin Update button
    Then Verify credit card Processor succesfully changed
    Then User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | Ypsilanti Vision, Washtenaw Avenue, Ypsilanti, MI 48197 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
  Scenario: TC 025 CC Order placed then Card Processor changed to PNP and a New CC order placed for Store Bryn Mawr
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Bryn Mawr|
  And user click payments
  Then verify by default chase is selected as CC processor else update to chase
  |chase| 
    Given User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | Bryn Mawr, North Bryn Mawr Avenue, Bryn Mawr, PA 19010 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    When user opens the ADMIN site
    And verify if user logged in
    And user clicks stores
    And user search for store
    |Bryn Mawr|
    And user click payments
    And user change Credit Card Processor to PNP
    |pnp|
    And user clicks Admin Update button
    Then Verify credit card Processor succesfully changed
    Then User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | Bryn Mawr, North Bryn Mawr Avenue, Bryn Mawr, PA 19010 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
   
  

