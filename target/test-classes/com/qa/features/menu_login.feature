@MenuLogin
Feature: Menu

  Background: 
    Given User opens the Website without clearing cache

@DevMenuLogin
  Scenario: Precondition - user logs in as a current customer
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | snigam@judge.com |
      | password | password         |
    And Click on the Log In button
    
@DevMenuLogin   
  Scenario: Verify user is able to access Menu Tab
    And user clicks on Menu tab
    Then user should be able to access Menu tab successfully

@DevMenuLogin
  Scenario: Verify menu images of all products are displayed on menu page
  And user clicks on Menu tab
Then all product menu images should be displayed